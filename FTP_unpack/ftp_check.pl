#!/usr/bin/perl
$|++;
## use cwd
use Cwd;
use Cwd 'abs_path';
use File::Basename;
## looking at this file : 
my $file = $ARGV[0];
## located in this directory
my $cwd = getcwd.dirname(abs_path($file));
$file = basename(abs_path($file));

## what is allowed?
my $allowed;

# allowed type: txt, gzip
$allowed{'text/plain'} = 1;
$allowed{'application/x-tar'} = 1;
$allowed{'application/x-tar-gz'} = 1;
$allowed{'application/x-gzip'} = 1;
$allowed{'application/zip'} = 1;
$allowed{'application/x-rar'} = 1;

print "Checking if uploaded file is of allowed MIME\n";
## wait for the main upload to finish.
my $filesize =  -s "$cwd/$file";
sleep 2;
while ($filesize != -s "$cwd/$file") {
	$filesize = -s "$cwd/$file";
	print "Upload not finished ($filesize bytes so far)\n";
	sleep 3
}

## start processing
&ProcessFile($file,$cwd);


## subroutines
sub ProcessFile {
	my $item = $_[0];
	my $basedir = $_[1];
	print "Working Dir: $basedir\n";
	print "Working Item = $item\n";
	my $mime = `cd $basedir && file --mime-type -b "$item"`;
	chomp($mime);
	print "Item Mime = $mime\n";
	if ($allowed{$mime} == 1) {
		print "  FileCheck '$item' : $mime => ok\n";
		if ("$basedir" ne "$cwd") {
			print "\t=>Moving to Base directory as ";
			my $target = $item;
			while (-e "$cwd/$target") {
				# append digit to prevent overwriting
				$target =~ m/(.*)\.(\w+)$/;
				my $name = $1;
				my $suffix = $2;
				if ($name =~ m/(.*\.v)(\d+)/) {
					$name = "$1".($2+1);
				}
				else {
					$name = "$name.v2";
				}
				$target = "$name.$suffix";
			}
			print "$target\n";
			`mv "$basedir/$item" "$cwd/$target"`;
			
		}
	}
	else {
		print "  $item => Filetype not allowed and will be discarded.\n";
		`rm -f "$item"`;
		
	}
	
}
