#!/usr/bin/perl
$|++;
## use cwd
use Cwd;
use Cwd 'abs_path';
use File::Basename;
## looking at this file : 
my $file = $ARGV[0];
## located in this directory
my $cwd = getcwd;#.dirname(abs_path($file));
my $file = basename(abs_path($file));

## what is allowed?
my $allowed;
my %unpack;

# allowed type: txt, gzip
$allowed{'text/plain'} = 1;
 
## unpack : tar, tar.gz, bzip, rar, zip
$unpack{'application/x-tar'} = 'tar xf';
$unpack{'application/x-tar-gz'} = 'tar xzf';
$unpack{'application/x-gzip'} = 'gunzip';
$unpack{'application/zip'} = 'unzip';
$unpack{'application/x-rar'} = 'rar x';

print "Checking if uploaded file is of allowed MIME\n";

## start processing
&ProcessFile($file,$cwd);


## subroutines
sub ProcessFile {
	my $item = $_[0];
	my $basedir = $_[1];
	print "Working Dir: $basedir\n";
	print "Working Item = $item\n";
	my $mime = `cd $basedir && file --mime-type -b "$item"`;
	chomp($mime);
	print "Item Mime = $mime\n";
	if ($allowed{$mime} == 1) {
		print "  FileCheck '$item' : $mime => ok\n";
		if ("$basedir" ne "$cwd") {
			print "\t=>Moving to Base directory as ";
			my $target = $item;
			while (-e "$cwd/$target") {
				# append digit to prevent overwriting
				$target =~ m/(.*)\.(\w+)$/;
				my $name = $1;
				my $suffix = $2;
				if ($name =~ m/(.*\.v)(\d+)/) {
					$name = "$1".($2+1);
				}
				else {
					$name = "$name.v2";
				}
				$target = "$name.$suffix";
			}
			print "$target\n";
			`mv "$basedir/$item" "$cwd/$target"`;
			
		}
	}
	else {
		if (exists($unpack{$mime})) {
			## make tmp dir
			my $rand = "unpack_".int(rand(500));
			`mkdir $basedir/$rand`;
			## move file (symlinking gave some issues...
			`mv "$basedir/$item" "$basedir/$rand/$item"`;
			my $dump = `cd $basedir/$rand && $unpack{$mime} $item`;
			my @files = `cd $basedir/$rand/ && ls `;
			foreach (@files) {
				chomp($_);
				my $unpacked = $_;
				next if ($unpacked eq $item) ;
				# check the unpacked file
				&ProcessFile($_,"$basedir/$rand");
			}
			## all done, remove the rand directory
			if (-d "$basedir/$rand" && "$basedir/$rand" ne "/" && "$basedir/$rand" ne "//") {
				$dump = `rm -Rf "$basedir/$rand"`;
			}
		}
		elsif ($mime eq 'application/x-directory') {
			## get files in subdirectory
			my @files = `cd $basedir/$item/ && ls `;
			foreach (@files) {
				chomp($_);
				# check the file
				&ProcessFile($_,"$basedir/$item");
			}
		}
		else {
			print "  $item => Filetype not allowed and will be discarded.\n";
			`rm -f "$item"`;
		}
	}
	
}
