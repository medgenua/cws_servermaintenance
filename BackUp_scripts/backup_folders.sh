#!/bin/bash

## read credentials file
source /CNV-WebStore/.Credentials/.credentials

echo " Backup the CNV-WebStore pages (without data)"
rsync -a --exclude 'data/*' --exclude 'datafiles/*' --del /CNV-WebStore /root/ServerBackup/CNV-WebStore/

#echo " Backup CNV-program directories (without data)"
##sync -a --exclude 'datafiles/*' --exclude 'data/*' --del $SCRIPTDIR/ /root/ServerBackup/CNVanalysis/
#rsync -a --delete /opt/QuantiSNP2 /root/ServerBackup/opt_quantisnp/

echo " Backup Genome Plots "
rsync -a --del $PLOTDIR/ /root/ServerBackup/CNVanalysis_plots/



#echo " Backup Server Maintenance scripts "
#rsync -a --del /opt/ServerMaintenance /root/ServerBackup/opt_ServerMaintenance/

