#!/bin/bash
source /CNV-WebStore/.Credentials/.credentials

# global mfold directories (any of primer design creates them)
# delete those first, so the next lines don't have to go over their contents.
find /tmp/ -maxdepth 1 -user "$SCRIPTUSER" -type d -mtime +1 -name '*mfold.files' -exec rm -Rf {} \;

# old cnv-related temp files
find /tmp/ -user "$SCRIPTUSER" -type f -mtime +2 -exec rm -f {} \;
find /tmp/ -user "$WWWRUN" -type f -mtime +2 -exec rm -f {} \;
find /var/tmp/site_uploads/ -mtime +180 -type f -exec rm -f {} \;
# old Bookmark and list files (kept for 6 months by default)
find $SITEDIR/bookmarks/ -mtime +180 -type f -exec rm -f {} \;
# old data dumps (tar.gz)
find $SITEDIR/data/ -mtime +14 -type f -name '*.tar.gz' -exec rm -f {} \;
# runtime status file
find $SITEDIR/status/ -mtime +30 -type f -exec rm -f {} \;

#QuantiSNPv2 Runtime Files
find $SCRIPTDIR/QuantiSNPv2/QS_QualityControl/ -mtime +30 -type f -exec rm -f {} \;
find $SCRIPTDIR/QuantiSNPv2/ -name '*.out' -mtime +30 -type f -exec rm -f {} \;
find $SCRIPTDIR/QuantiSNPv2/QS_output/  -mtime +30 -type f -exec rm -f {} \;
find $SCRIPTDIR/QuantiSNPv2/results_list/ -mtime +30 -type f -exec rm -f {} \;
find $SCRIPTDIR/QuantiSNPv2/results_xml/ -mtime +30 -type f -exec rm -f {} \;
find $SCRIPTDIR/QuantiSNPv2/status/ -mtime +30 -type f -exec rm -f {} \;
find $SCRIPTDIR/QuantiSNPv2/rawcnv/ -mtime +30 -type f -exec rm -f {} \;

#PennCNV Runtime Files
find $SCRIPTDIR/PennCNV/results_list/ -mtime +30 -type f -exec rm -f {} \;
find $SCRIPTDIR/PennCNV/results_xml/ -mtime +30 -type f -exec rm -f {} \;
find $SCRIPTDIR/PennCNV/ -name '*.out' -mtime +30 -type f -exec rm -f {} \;
find $SCRIPTDIR/PennCNV/rawcnv/ -mtime +30 -type f -exec rm -f {} \;
find $SCRIPTDIR/PennCNV/output/ -mtime +30 -type f -exec rm -f {} \;
find $SCRIPTDIR/PennCNV/log/ -mtime +30 -type f -exec rm -f {} \;
find $SCRIPTDIR/PennCNV/status/ -mtime +30 -type f -exec rm -f {} \;
#VanillaICE Runtime Files
find $SCRIPTDIR/VanillaICE/output/ -mtime +30 -type f -exec rm -f {} \;
find $SCRIPTDIR/VanillaICE/rawcnv/ -mtime +30 -type f -exec rm -f {} \;
find $SCRIPTDIR/VanillaICE/results_list/ -mtime +30 -type f -exec rm -f {} \;
find $SCRIPTDIR/VanillaICE/results_xml/ -mtime +30 -type f -exec rm -f {} \;
find $SCRIPTDIR/VanillaICE/ -name '*.out' -mtime +30 -type f -exec rm -f {} \;
find $SCRIPTDIR/VanillaICE/status/ -mtime +30 -type f -exec rm -f {} \;

# ADD PED & CNVanalysis output files
find $SCRIPTDIR/ -maxdepth 1 -mtime +10 -type f -name '*.out' -exec rm -f {} \;
find $SCRIPTDIR/ -maxdepth 1 -mtime +10 -type f -name '*_fortrack.txt' -exec rm -f {} \;

# PrimerDesign Runtime Files (gDNA)



# PrimerDesign Runtime Files (exon-exon junctions)




# PrimerDesign Runtime Files (intron spanning)




# southern probe design output files




