#!/bin/bash
echo "Copying backups to readynas"
mount -t cifs //NAS-Drive /root/mountpoint -o credentials=/root/.samba_cred,lbf
rsync -rtvW --del --modify-window=2 --exclude-from 'rsync_excludes.txt' /root/ServerBackup/ /root/mountpoint/backup_server/
umount /root/mountpoint
echo "Backup complete";
