#!/usr/bin/perl
use Getopt::Std;
use DBI;
$| = 1;

# read credentials
use Cwd 'abs_path';
$credpath = abs_path("../LoadCredentials.pl");
require($credpath);

####################
# DEFINE VARIABLES #
####################
my %chromhash = ();
my @chroms = (1..22);
foreach (@chroms)  {
	$chromhash{ $_ } = $_;
}
$chromhash{ "X" } = 23;
$chromhash{ "Y" } = 24;
$chromhash{ "M" } = "25";
my %InfoByGeneID;
##########################
# COMMAND LINE ARGUMENTS #
##########################
# L = Looking at a new database, LiftOver old values if needed
# D = Use specific database (mandatory as target db if L is specified), otherwise use default.
# d = Database to use as source (mandatory if L is specified.
# U = Just update number of disrupted genes for all aberrations in the database.
# u = User ID, mandatory
# S = Just recreate summary table & update aberrations (daily, for pubmed citations, omim data, etc)
getopts('SLUD:u:d:', \%opts);  # option are in %opts

# print name of script and what will happen
print "#########################################\n";
print "# UPDATING REFSEQ GENE ANNOTATION TABLE #\n";
print "#########################################\n";
print "\n";


###################################
# CONNECT TO CNVanalysis DATABASE #
###################################
my $db = "";

print "1/ Connecting to the database\n";
my $ucscbuild;
if ($opts{'D'}){
	$db = "CNVanalysis".$opts{'D'};
	$targetdb = $opts{'D'};
	$ucscbuild = $opts{'D'};
	$ucscbuild =~ s/^-//;
	print "\t=> Using user specified database $db\n";
}
else {
	# connect to GenomicBuilds DB to get the current Genomic Build Database. 
	my $dbhgb = DBI->connect("dbi:mysql:GenomicBuilds:$host",$userid,$userpass);
	my $gbsth =$dbhgb->prepare("SELECT name, StringName FROM CurrentBuild");
	$gbsth->execute();
	my @row = $gbsth->fetchrow_array();
	$gbsth->finish();
	$db = "CNVanalysis".$row[0];
	$newbuild = $row[1];
	$ucscbuild = $row[0];
	$ucscbuild =~ s/^-//;
	print "\t=> Using current build database (build $newbuild)\n";	
}
#$db = "CNVtesting";
$connectionInfocnv="dbi:mysql:$db:$host"; 
$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
if ($opts{'d'}) {
	$sourcedb = $opts{'d'};
}
if ($opts{'u'}) {
	$uid = $opts{'u'};
}
else {
	$uid = 1;
}
# TASK WAS ONLY TO UPDATE THE ABERRATIONS TABLE 
if ($opts{'U'}) {
	print "2/ Updating Aberrations table (number of genes disrupted for each aberration)\n";
	my $abquery = "SELECT id, chr, start, stop, nrgenes FROM aberration ORDER BY chr, start, stop";
	$absth = $dbh->prepare($abquery);
	my $rowcache;
	my $rows;
	$rows = $absth->execute();
	print "\t=> Updating $rows CNVs, this is going to take a while...\n";
	print "\tChromosome"; 
	if ($rows < 10000) {
		$max = $rows;
	}
	else {
		$max = 10000;
	}
	$prevstart = 0;
	$prevstop = 0;
	$prevchr = 0;
	$prevgenes = 0;
	# prepare queries
	my $genequery = "SELECT COUNT(symbol) FROM genesum WHERE chr = ? AND (((start BETWEEN ? AND ? ) OR (stop BETWEEN ? AND ?)) OR (start <= ? AND stop >= ?))";
	$genesth = $dbh->prepare($genequery);
	my $updatequery = "UPDATE aberration SET nrgenes = ? WHERE id = ?";
	$updatesth = $dbh->prepare($updatequery);
	while (my $result = shift(@$rowcache) || shift( @{$rowcache = $absth->fetchall_arrayref(undef,$max)|| []})) {
		my $id = $result->[0];
		my $chr = $result->[1];
		my $start = $result->[2];
		my $stop = $result->[3];
		my $onrg = $result->[4];
		if ($chr != $prevchr) {
			print " $chr";
		}
		if ($chr == $prevchr && $start == $prevstart && $stop == $prevstop && $prevnrgenes != $onrg) {
			# identical, update and continue
			$updatesth->execute($prevnrgenes,$id);
			next;
		}
		#$genesth = $dbh->prepare($genequery);
		$genesth->execute($chr,$start,$stop,$start,$stop,$start,$stop);
		my @count = $genesth->fetchrow_array();
		#$genesth->finish(); 
		my $nrgenes = $count[0];
		if ($nrgenes != $onrg) {
			$updatesth->execute($nrgenes,$id);
			#$dbh->do($updatequery);
		}
		$prevchr = $chr;
		$prevstart = $start;
		$prevstop = $stop;
		$prevnrgenes = $nrgenes;
	}
	$genesth->finish();
	$updatesth->finish();
	print "\n";
	print "################\n";
	print "# ALL FINISHED #\n";
	print "################\n";
}

####################################
# TASK ONLY RECREATE SUMMARY TABLE #
#################################### 
elsif ($opts{'S'}) {
	print "2/ Downloading the datafiles for summary creation\n";
	##########################
	# GET UPDATED GENE LISTS #
	##########################
	my $urlsth = $dbh->prepare("SELECT item, url FROM Administration WHERE UpdateScript = 'update_refseq.pl'");
	my $urlrows = $urlsth->execute();
	my $failed = 0;
	if ($urlrows == 0) {
		print "\t=> No URLs specified\n";
		$failed++;
	}
	while (my @array = $urlsth->fetchrow_array()) {
		my $url = $array[1];
		my $item = $array[0];

		if ($url eq 'LIFT' || $url eq '') {
			print " NO URL ('lift')\n";
			$failed = 1;
			next;
		}
		print "\t$item from $url : ";
		if ($item eq 'OmimMorbidMap') {
			print("Skipping omim morbid\n");
			next;
		}
		if ($url !~ m/\.gz$/i) {
			`wget --no-check-certificate -q -O '/tmp/$item.txt' '$url' && chmod 777 '/tmp/$item.txt'`;
		}
		else {
			`wget --no-check-certificate -q -O '/tmp/$item.txt.gz' '$url' && gunzip '/tmp/$item.txt.gz' --force && chmod 777 '/tmp/$item.txt'`;
		}
		if (-z "/tmp/$item.txt" || -e "/tmp/$item.txt.gz") {
			print " FAILED\n";
			$failed = 1;
		}
		else {
			print " OK\n";
		}
	}
	if ($failed == 1) {
		print "\n";
		print "####################################\n";
		print "# DOWNLOAD FAILED => SHUTTING DOWN #\n";
		print "####################################\n";
		die("DOWNLOAD FAILED");
	}
	
    #################
    ## GENE INFO : ##
    #################
    #  - from ncbi
    #  - symbol + synonyms
    #  - coding / pseudo / ... 
	# Read the gene_info : multiple info fields.
	print "4/ Reading gene_info table into hash\n";
    
	open IN, "/tmp/gene_info.txt";
	#open IN, "gene_info";
	while (<IN>) {
		chomp($_);
		my @p = split(/\t/,$_);
		if ($p[0] == 9606) {   # 9606 is taxonomy id for homo sapiens !
			my $geneid = $p[1];
			$InfoByGeneID{$geneid}{'stored'} = 0;
			$InfoByGeneID{$geneid}{'symbol'} = $p[2];
			$InfoByGeneID{$geneid}{'synonym'} = $p[4];
			$InfoByGeneID{$geneid}{'chr'} = $p[6];
			$InfoByGeneID{$geneid}{'type'} = $p[9]; # coding, pseudo, ...
			# use official nomenclature symbol if available
			if ($p[10] ne '-') {
				$InfoByGeneID{$geneid}{'symbol'} = $p[10];
			}
			else {
				$InfoByGeneID{$geneid}{'symbol'} = $p[2];
			}
			# use official nomenclature full name if available
			if ($p[11] ne '-') {
				$InfoByGeneID{$geneid}{'name'} = $p[11];
			}
			else {
				$InfoByGeneID{$geneid}{'name'} = $p[8];
			}
			## store dbrefs
			if ($p[5] ne '-') {
				my @q = split(/\|/,$p[5]);
				foreach (@q) {
					my ($ref,$refid) = split(/:/,$_);
					#if ($ref =~ m/vega/i) {
					#	$InfoByGeneID{$geneid}{'vegaID'} = $refid;
					#}
					#els
                    if ($ref =~ m/imgt/i) {
						$InfoByGeneID{$geneid}{'imgtID'} = $refid;
					}
					elsif ($ref =~ m/ensembl/i) {
						$InfoByGeneID{$geneid}{'egID'} = $refid;
					}
					elsif ($ref =~ m/rgd/i) {
						$InfoByGeneID{$geneid}{'rgdID'} = $refid;
					}
					elsif ($ref =~ m/mim/i) {
						$InfoByGeneID{$geneid}{'omimID'} = $refid;
					}
					elsif ($ref =~ m/mirbase/i) {
						$InfoByGeneID{$geneid}{'mirbaseID'} = $refid;
					}
					elsif ($ref =~ m/^hgnc/i) {
						$InfoByGeneID{$geneid}{'hgncID'} = $refid;
					}
					elsif ($ref =~ m/hprd/i) {
						$InfoByGeneID{$geneid}{'hprdID'} = $refid;
					}
				}
			}

		}
	}
	close IN;
	unlink('/tmp/gene_info.txt');
	

	print "3/ Create Summarised Gene Table\n";
	&Summarise;

	# UPDATE ABERRATIONS TABLE
	print "4/ Updating Aberrations table (number of genes disrupted for each aberration)\n";
	my $abquery = "SELECT id, chr, start, stop,nrgenes FROM aberration ORDER BY chr, start, stop";
	$absth = $dbh->prepare($abquery);
	my $rowcache;
	my $rows;
	$rows = $absth->execute();
	print "\t=> Updating $rows CNVs, this is going to take a while...\n";
	print "\tChromosome";
	if ($rows < 10000) {
		$max = $rows;
	}
	else {
		$max = 10000;
	}
	$prevstart = 0;
	$prevstop = 0;
	$prevchr = 0;
	$prevgenes = 0;
	$cstart = 0;
	$cstop = 0;
	$cchr = 1;
	# prepare queries
	my $genequery = "SELECT COUNT(symbol) FROM genesum WHERE chr = ? AND (((start BETWEEN ? AND ? ) OR (stop BETWEEN ? AND ?)) OR (start <= ? AND stop >= ?))";
	$genesth = $dbh->prepare($genequery);
	my $updatequery = "UPDATE aberration SET nrgenes = ? WHERE id = ?";
	$updatesth = $dbh->prepare($updatequery);


	while (my $result = shift(@$rowcache) || shift( @{$rowcache = $absth->fetchall_arrayref(undef,$max)|| []})) {
		my $id = $result->[0];
		my $chr = $result->[1];
		my $start = $result->[2];
		my $stop = $result->[3];
		my $onrg = $result->[4];
		if ($chr != $prevchr) {
			print " $chr";
		}
		if ($chr == $prevchr && $start == $prevstart && $stop == $prevstop && $prevnrgenes != $onrg) {
			# identical, update and continue
			$updatesth->execute($prevnrgenes,$id);
			#$dbh->do($updatequery);
			next;
		}
		$genesth->execute($chr,$start,$stop,$start,$stop,$start,$stop);
		#$genesth = $dbh->prepare($genequery);
		#$genesth->execute();
		my @count = $genesth->fetchrow_array();
		#$genesth->finish(); 
		my $nrgenes = $count[0];
		if ($nrgenes != $onrg) {
			$updatesth->execute($nrgenes,$id);
		}
		#$dbh->do($updatequery);
		$prevchr = $chr;
		$prevstart = $start;
		$prevstop = $stop;
		$prevnrgenes = $nrgenes;
	}
	$updatesth->finish();
	$genesth->finish();
	print "\n";
	print "###################\n";
	print "# UPDATE FINISHED #\n";
	print "###################\n";

}

#############################
# TASK IS FULL TABLE UPDATE #
#############################
else {
    
	print "2/ Downloading the datafiles\n";
	##########################
	# GET UPDATED GENE LISTS #
	##########################
	my $urlsth = $dbh->prepare("SELECT item, url FROM Administration WHERE UpdateScript = 'update_refseq.pl'");
	my $urlrows = $urlsth->execute();
	my $failed = 0;
	if ($urlrows == 0) {
		print "\t=> No URLs specified\n";
		$failed++;
	}
	print "rows found: $urlrows\n";
	while (my @array = $urlsth->fetchrow_array()) {
		my $url = $array[1];
		my $item = $array[0];
		if ($url eq 'LIFT' || $url eq '') {
			print " NO URL ('lift')\n";
			$failed = 1;
			next;
		}
		print "\t$item from $url : ";
		if ($item eq 'OmimMorbidMap') {
			print(" skip\n");
			next;
		}
		if ($url !~ m/\.gz$/i) {
			`wget --no-check-certificate -q -O '/tmp/$item.txt' '$url' && chmod 777 '/tmp/$item.txt'`;
		}
		else {
			`wget --no-check-certificate -q -O '/tmp/$item.txt.gz' '$url' && gunzip '/tmp/$item.txt.gz' --force && chmod 777 '/tmp/$item.txt'`;
		}
		if (-z "/tmp/$item.txt" || -e "/tmp/$item.txt.gz") {
			print " FAILED\n";
			$failed = 1;
		}
		else {
			print " OK\n";
		}
	}
	if ($failed == 1) {
		if ($opts{'L'}) {
			# perform liftover!
			# first check all needed info	
			if ($sourcedb eq '' || $targetdb eq '') {
				print "\t##############################################\n";
				print "\t# INSUFFICIENT INFO FOR LIFT OVER COMMAND    #\n";
				print "\t#  => source (d) or target (D) build missing #\n";
				print "\t##############################################\n";
				print "\n";
				print "\t################\n";
				print "\t# NOT UPDATING #\n";
				print "\t################\n";
				die("NOT UPDATING");
			}
			# ok, start lift over command.
			print "\t##############################\n";
			print "\t# STARTING LIFT OVER PROCESS #\n";
			print "\t##############################\n";

			system("cd $maintenancedir/LiftOver/ && ./LiftOver.pl -U $uid -d '$sourcedb' -D '$targetdb' -T genes");
			# summarise data
			print "Final Step: Summarising table\n";
			&Summarize;
			# update aberrations
			system("./update_refseq.pl -U -D '$targetdb' -u $uid");
			print "\n";
			print "###################\n";
			print "# UPDATE FINISHED #\n";
			print "###################\n";
			exit;

		}
		else {
			print "\n";
			print "\t####################################################\n";
			print "\t# SOME FILES FAILED TO DOWNLOAD, CHECK YOUR URLs ! #\n";
			print "\t####################################################\n";
			print "\n";
			print "\t################\n";
			print "\t# NOT UPDATING #\n";
			print "\t################\n";
			die("SOME FILES FAILED TO DOWNLOAD, CHECK YOUR URLs");
		}
	}
	print "\t=> All files successfully downloaded.\n";
	########################################
	# EVERYTHING DOWNLOADED, START PARSING #
	########################################
	# Read the reflink table into hash  => transcript to geneID
	my %reflink;
	print "3/ Reading the reflink table into hash\n";
	open IN, "/tmp/refLink.txt";
	while (<IN>) {
		my $line = $_;
		chomp($line);
		my @pieces = split(/\t/,$line);
        if ($pieces[0] != 9606) {
            next;
        }
		my $geneid = $pieces[1];	# ncbi-gene ID
		my ($nm) = split(/\./,$pieces[3],2);		# NM_  transcript, no version
		$reflink{$nm} = $geneid;
		#$InfoByGeneID{$geneid}{'transcript'} = $nm;
	}
	close IN;
	print "\t=> Done.\n";
	unlink('/tmp/refLink.txt') ;

	# Read the gene_info : multiple info fields.
	print "4/ Reading gene_info table into hash\n";
	open IN, "/tmp/gene_info.txt";
	while (<IN>) {
		chomp($_);
		my @p = split(/\t/,$_);
		if ($p[0] == 9606) {   # 9606 is taxonomy id for homo sapiens !
			my $geneid = $p[1];
			$InfoByGeneID{$geneid}{'stored'} = 0;
			$InfoByGeneID{$geneid}{'symbol'} = $p[2];
			$InfoByGeneID{$geneid}{'synonym'} = $p[4];
			$InfoByGeneID{$geneid}{'chr'} = $p[6];
			$InfoByGeneID{$geneid}{'type'} = $p[9]; # coding, pseudo, ...
			# use official nomenclature symbol if available
			if ($p[10] ne '-') {
				$InfoByGeneID{$geneid}{'symbol'} = $p[10];
			}
			else {
				$InfoByGeneID{$geneid}{'symbol'} = $p[2];
			}
			# use official nomenclature full name if available
			if ($p[11] ne '-') {
				$InfoByGeneID{$geneid}{'name'} = $p[11];
			}
			else {
				$InfoByGeneID{$geneid}{'name'} = $p[8];
			}
			## store dbrefs
			if ($p[5] ne '-') {
				my @q = split(/\|/,$p[5]);
				foreach (@q) {
					my ($ref,$refid) = split(/:/,$_);
					if ($ref =~ m/vega/i) {
						$InfoByGeneID{$geneid}{'vegaID'} = $refid;
					}
					elsif ($ref =~ m/imgt/i) {
						$InfoByGeneID{$geneid}{'imgtID'} = $refid;
					}
					elsif ($ref =~ m/ensembl/i) {
						$InfoByGeneID{$geneid}{'egID'} = $refid;
					}
					elsif ($ref =~ m/rgd/i) {
						$InfoByGeneID{$geneid}{'rgdID'} = $refid;
					}
					elsif ($ref =~ m/mim/i) {
						$InfoByGeneID{$geneid}{'omimID'} = $refid;
					}
					elsif ($ref =~ m/mirbase/i) {
						$InfoByGeneID{$geneid}{'mirbaseID'} = $refid;
					}
					elsif ($ref =~ m/hgnc/i) {
						$InfoByGeneID{$geneid}{'hgncID'} = $refid;
					}
					elsif ($ref =~ m/hprd/i) {
						$InfoByGeneID{$geneid}{'hprdID'} = $refid;
					}
				}
			}

		}
	}
	close IN;
	unlink('/tmp/gene_info.txt');
	
	# READ REFGENE
    ## 
	# refgene table contains multiple transcripts. So read this and store immediately.
	my $trunc = "TRUNCATE TABLE genes";
	$dbh->do($trunc);
	open IN, "/tmp/refGene.txt";
	my $nrgenes = 0;
	print "5/ Inserting gene information\n";
	# prepare query
	my $query = "INSERT INTO genes (symbol, chr, start, end, strand, nmID, egID, omimID, geneID, vegaID, imgtID, rgdID, mirbaseID, hgncID, hprdID, nrexons, exonstarts, exonends,cdsstart,cdsstop) values (?, ?, ?, ?, ?, ?,?, ?, ?, ?,?,?,?,?,?,?,?,?,?,?) ";
	$sth =  $dbh->prepare($query);
	# process data
	while (<IN>) {
		my $line = $_;
		chomp($line);
		my @pieces = split(/\t/,$line);
		if ($pieces[2] =~ m/_/ ) {
			# skip non-canonical chromosomes (chr6_random etc)
			next;
		}
        
		else {
			# info from ncib/refgene file
			my $chrom = $pieces[2];		
			my $start = $pieces[4];
			my $end = $pieces[5];
			my $strand = $pieces[3];
			my $nmname = $pieces[1];	#transcript
            my ($nmname) = split(/\./,$pieces[1],2);
            #print("$nmname_full => $nmname\n");
			$chrom =~ s/chr//;
			$chrom = $chromhash{ $chrom };	#X->23 ; Y->24
			my $exoncount = $pieces[8];
			my $exonstarts = $pieces[9];
			my $exonends = $pieces[10];
			my $cdsstart = $pieces[6];
			my $cdsstop = $pieces[7];
			# get gene_id based on transcript from reflink file
			my $gid = '';
			if ($reflink{$nmname}) {
				 $gid = $reflink{$nmname};
			}
			else {
                #if ($nmname =~ m/^NM/) {
				    print "$nmname is not in reflink hash\n";
                    
                #}
				next;
			}	
			# store data
			if ($InfoByGeneID{$gid}) {
                
				# use info from gene_info 
				#(symbol, chr, start, end, strand, nmID, egID, omimID, geneID, vegaID, imgtID, rgdID, mirbaseID, hgncID, hprdID, nrexons, exonstarts, exonends)
				$sth->execute($InfoByGeneID{$gid}{'symbol'}, $chrom, $start, $end, $strand, $nmname,$InfoByGeneID{$gid}{'egID'},$InfoByGeneID{$gid}{'omimID'},$gid, $InfoByGeneID{$gid}{'vegaID'},$InfoByGeneID{$gid}{'imgtID'},$InfoByGeneID{$gid}{'rgdID'},$InfoByGeneID{$gid}{'mirbaseID'},$InfoByGeneID{$gid}{'hgncID'},$InfoByGeneID{$gid}{'hprdID'},$exoncount, $exonstarts, $exonends,$cdsstart,$cdsstop);
				$InfoByGeneID{$gid}{'stored'} = 1;
			}
			else {
				my $symbol = $pieces[12];
                
				$sth->execute($symbol, $chrom, $start, $end, $strand, $nmname,$InfoByGeneID{$gid}{'egID'},$InfoByGeneID{$gid}{'omimID'},$gid, $InfoByGeneID{$gid}{'vegaID'},$InfoByGeneID{$gid}{'imgtID'},$InfoByGeneID{$gid}{'rgdID'},$InfoByGeneID{$gid}{'mirbaseID'},$InfoByGeneID{$gid}{'hgncID'},$InfoByGeneID{$gid}{'hprdID'},$exoncount, $exonstarts, $exonends,$cdsstart,$cdsstop);
				#print "$line\n";
			}
			$nrgenes++;
		}
	}
	$sth->finish();
	close IN;
	unlink('/tmp/refGene.txt');
	print "\t=> $nrgenes entries\n";
	# read gene2ensembl
	open IN, "/tmp/gene2ensembl.txt";
	#open IN, "gene2ensembl";
	print "6/ Inserting gene to ensembl links\n";
	# prepare query
	$query = "UPDATE genes SET egID = ? , etID = ? , epID = ? , npID = ? WHERE geneID = ? AND nmID = ? ";
	$sth =  $dbh->prepare($query);
	# process data
	while (<IN>) {
		my $line = $_;
		chomp($line);
		my @p = split(/\t/,$line);
		if ($p[0] != 9606 ) {
			# non-human
			next;
		}
		# info from refgene file
		my $geneid = $p[1];		
		my $egid = $p[2];
		my $nmid = $p[3];
		my $etid = $p[4];
		my $npid = $p[5];
		my $epid = $p[6];
		# trim .\d from end of nm
		$nmid =~ s/(.*)(\.\d+)$/$1/;
		# update table
		$sth->execute($egid,$etid,$epid,$npid,$geneid,$nmid);
	}
	$sth->finish();
	close IN;
	unlink("/tmp/gene2ensembl.txt");
	
    # get mane status.
	open IN, "/tmp/mane.txt";
	print "7/ Add MANE status\n";
	# prepare query
	$query = "UPDATE genes SET mane = ? WHERE geneID = ? AND nmID = ? ";
	$sth =  $dbh->prepare($query);
	# process data
	while (<IN>) {
		my $line = $_;
		chomp($line);
		my @p = split(/\t/,$line);
        
		if ($p[0] =~ m/^#/) {
			next;
		}
		# info from refgene file
		my ($dummy,$geneid) = split(/:/,$p[0],2);
		my ($nmid) = split(/\./,$p[5],2);
        my $status = $p[9];
		#print("$geneid => $nmid => $status\n");
		# update table
		$sth->execute($status, $geneid, $nmid);
	}
	$sth->finish();
	close IN;
	unlink("/tmp/mane.txt");
			
	# CREATE A SUMMARISED TABLE FOR GENES 
	print "7/ Create Summarised Gene Table\n";
	&Summarise;
	# UPDATE ABERRATIONS TABLE
	print "8/ Updating Aberrations table (number of genes disrupted for each aberration)\n";
	my $abquery = "SELECT id, chr, start, stop FROM aberration ORDER BY chr, start, stop";
	$absth = $dbh->prepare($abquery);
	my $rowcache;
	my $rows;
	$rows = $absth->execute();
	print "\t=> Updating $rows CNVs, this is going to take a while...\n";
	print "\tChromosome";
	if ($rows < 10000) {
		$max = $rows;
	}
	else {
		$max = 10000;
	}
	$prevstart = 0;
	$prevstop = 0;
	$prevchr = 0;
	$prevgenes = 0;
	$cstart = 0;
	$cstop = 0;
	$cchr = 1;

	while (my $result = shift(@$rowcache) || shift( @{$rowcache = $absth->fetchall_arrayref(undef,$max)|| []})) {
	#while(@result = $absth->fetchrow_array()) {
		my $id = $result->[0];
		my $chr = $result->[1];
		my $start = $result->[2];
		my $stop = $result->[3];
		if ($chr != $prevchr) {
			print " $chr";
		}
		if ($chr == $prevchr && $start == $prevstart && $stop == $prevstop && $chr != 0 && $start != 0 && $stop != 0) {
			# identical, update and continue
			my $updatequery = "UPDATE aberration SET nrgenes='$prevnrgenes' WHERE id='$id'";
			$dbh->do($updatequery);
			next;
		}
		my $genequery = "SELECT COUNT(symbol) FROM genesum WHERE chr='$chr' AND (((start BETWEEN $start AND $stop) OR (stop BETWEEN $start AND $stop)) OR (start <= $start AND stop >= $stop))";
		$genesth = $dbh->prepare($genequery);
		$genesth->execute();
		my @count = $genesth->fetchrow_array();
		$genesth->finish(); 
		my $nrgenes = $count[0];
		my $updatequery = "UPDATE aberration SET nrgenes='$nrgenes' WHERE id='$id'";
		$dbh->do($updatequery);
		$prevchr = $chr;
		$prevstart = $start;
		$prevstop = $stop;
		$prevnrgenes = $nrgenes;
	}
	# UPDATE CUS_ABERRATIONS TABLE
	print "\n16/ Updating CUS_Aberrations table (number of genes disrupted for each aberration)\n";
	$abquery = "SELECT id, chr, start, stop FROM cus_aberration ORDER BY chr, start, stop";
	$absth = $dbh->prepare($abquery);
	#my $rowcache;
	#my $rows;
	$rows = $absth->execute();
	print "\t=> Updating $rows CNVs, this is going to take a while...\n";
	print "\tChromosome";
	if ($rows < 10000) {
		$max = $rows;
	}
	else {
		$max = 10000;
	}
	$prevstart = 0;
	$prevstop = 0;
	$prevchr = 0;
	$prevgenes = 0;
	$cstart = 0;
	$cstop = 0;
	$cchr = 1;

	while (my $result = shift(@$rowcache) || shift( @{$rowcache = $absth->fetchall_arrayref(undef,$max)|| []})) {
	#while(@result = $absth->fetchrow_array()) {
		my $id = $result->[0];
		my $chr = $result->[1];
		my $start = $result->[2];
		my $stop = $result->[3];
		if ($chr != $prevchr) {
			print " $chr";
		}
		if ($chr == $prevchr && $start == $prevstart && $stop == $prevstop && $chr != 0 && $start != 0 && $stop != 0) {
			# identical, update and continue
			my $updatequery = "UPDATE cus_aberration SET nrgenes='$prevnrgenes' WHERE id='$id'";
			$dbh->do($updatequery);
			next;
		}
		my $genequery = "SELECT COUNT(symbol) FROM genesum WHERE chr='$chr' AND (((start BETWEEN $start AND $stop) OR (stop BETWEEN $start AND $stop)) OR (start <= $start AND stop >= $stop))";
		$genesth = $dbh->prepare($genequery);
		$genesth->execute();
		my @count = $genesth->fetchrow_array();
		$genesth->finish(); 
		my $nrgenes = $count[0];
		my $updatequery = "UPDATE cus_aberration SET nrgenes='$nrgenes' WHERE id='$id'";
		$dbh->do($updatequery);
		$prevchr = $chr;
		$prevstart = $start;
		$prevstop = $stop;
		$prevnrgenes = $nrgenes;
	}

	print "\n";
	print "###################\n";
	print "# UPDATE FINISHED #\n";
	print "###################\n";

}


sub Summarise {
	print "\t=>Fill Summary table\n";
	my $trunc = "TRUNCATE TABLE genesum";
	$dbh->do($trunc);
	my $sumquery = "SELECT chr, start, end, strand, symbol, exonstarts, exonends, geneID, egID,cdsstart,cdsstop FROM genes ORDER BY symbol, chr, start ";
	my $sumsth = $dbh->prepare($sumquery);
	$sumsth->execute();
	$firststart = 0;
	$laststop = 0;
	$firstcdsstart = 0;
	$lastcdsstop = 0;
	$prevchr = 1;
	$prevsymb = '';
	$prevgeneid = '';
	$prevegid = '';
	$entries = 0;
	$prevstrand = '';
    $morbidtxt = '';
	my %exonhash; 
	my $query = "INSERT INTO genesum (chr, start, stop, strand, symbol,exonstarts,exonends, geneID, geneTXT, synonyms, omimID, egID, cdsstart, cdsstop, morbidTXT) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	$sth = $dbh->prepare($query);
	while (my @row = $sumsth->fetchrow_array()) {
		my $chr = $row[0];
		my $chrtxt = $chromhash{ $chr };
		my $start = $row[1];
		my $stop = $row[2];
		my $strand = $row[3];
		my $symbol = $row[4];
		my $exonstart = $row[5];
		my $exonend = $row[6];
		my $geneid = $row[7];
		my $egid = $row[8];
		my $cdsstart = $row[9];
		my $cdsstop = $row[10];
		my @exonstarts = split(/,/,$exonstart);
		my @exonends = split(/,/,$exonend);
		## 1. new symbol
		if ($symbol ne $prevsymb) {
			# need to write out previous symbol !
			if ($prevsymb ne '') {
				#prepare exons strings
				my $exstartstring = '';
				my $exendstring = '';
				foreach my $key (sort keys %exonhash) {
					$exstartstring .= "$key,";
					$exendstring .= $exonhash{$key} . ",";
				}
				$exstartstring = substr($exstartstring,0,-1);
				$exendstring = substr($exendstring,0,-1);
				# get synonyms and names
				my $syns = $InfoByGeneID{$prevgeneid}{'synonym'};
				my $name = $InfoByGeneID{$prevgeneid}{'name'};
				my $omim = $InfoByGeneID{$prevgeneid}{'omimID'};
				# update
				$sth->execute($prevchr, $firststart, $laststop, $prevstrand, $prevsymb, $exstartstring,$exendstring,$prevgeneid,$name,$syns,$omim,$prevegid,$firstcdsstart,$lastcdsstop, $morbidtxt);
				#print "inserted : $prevgeneid ($name ; $syns, $exstartstring)\n";
				%exonhash = ();
			}
			$prevstart = $start;
			$firststart = $start;
			$firstcdsstart = $cdsstart;
			$prevstop = $stop;
			$laststop = $stop;
			$lastcdsstop = $cdsstop;
			$prevchr = $chr;
			$prevsymb = $symbol;
			$prevstrand = $strand;
			$prevgeneid = $geneid;
			$prevegid = $egid;
			my $idx = 0;
			foreach(@exonstarts) {
				#if (!exists($exonhash{$_})) {
				#	$exonhash{$_} = $exonends[$idx];
				#}
				#if ($exonhash{$_} < $exonends[$idx] || !defined($exonhash{$_})) {
				#	$exonhash{$_} = $exonends[$idx];
				#}
				$hit = 0;
				foreach(keys(%exonhash)) {
					## this exon replaces the one in the hash.
					if ($exonstarts[$idx] <= $_ && $exonends[$idx] >= $exonhash{$_}) {
						delete($exonhash{$_});
						$exonhash{$exonstarts[$idx]} = $exonends[$idx];
						$hit++;
					}
					## this exon extends the one in the hash 3'
					elsif ($exonstarts[$idx] >= $_ && $exonstarts[$idx] <= $exonhash{$_} && $exonends[$idx] > $exonhash{$_}) {
						$exonhash{$_} = $exonends[$idx];
						$hit++;
					}
					## this exon extends the one in the hash 5'
					elsif ($exonstarts[$idx] <= $_ && $exonends[$idx] <= $exonhash{$_} && $exonends[$idx] >= $_) {
						my $e = $exonhash{$_};
						delete($exonhash{$_});

						$exonhash{$exonstarts[$idx]} = $e;#xonhash{$_};
						$hit++;
					}
				}
				if ($hit == 0) {
					 $exonhash{$exonstarts[$idx]} = $exonends[$idx];
				}
				$idx++;
			}
			$entries = 1;
			next; # gene
		}
		## 2. same symbol, non-overlapping transcript variant and not first item processed.
		if ($start > $laststop && $laststop != 0) {
			if ($prevsymb ne '') {
				my $exstartstring = '';
				my $exendstring = '';
				foreach my $key (sort keys %exonhash) {
					$exstartstring .= "$key,";
					$exendstring .= $exonhash{$key} . ",";
				}
				$exstartstring = substr($exstartstring,0,-1);
				$exendstring = substr($exendstring,0,-1);
				# get synonyms and names
				my $syns = $InfoByGeneID{$prevgeneid}{'synonym'};
				my $name = $InfoByGeneID{$prevgeneid}{'name'};
				my $omim = $InfoByGeneID{$prevgeneid}{'omimID'};
				
				#print "inserted 2: $prevgeneid ($name ; $syns)\n";
				
				$sth->execute($prevchr, $firststart, $laststop, $prevstrand, $prevsymb, $exstartstring,$exendstring,$prevgeneid,$name,$syns,$omim,$prevegid,$firstcdsstart,$lastcdsstop, $morbidtxt);

			}
			$firststart = $start;
			$laststop = $stop;
			$firstcdsstart = $cdsstart;
			$lastcdsstop = $cdsstop;
			my $idx = 0;
			foreach(@exonstarts) {
				#if ($exonhash{$_} < $exonends[$idx] || !defined($exonhash{$_})) {
				#	$exonhash{$_} = $exonends[$idx];
				#}
				$hit = 0;
				foreach(keys(%exonhash)) {
					## this exon replaces the one in the hash.
					if ($exonstarts[$idx] <= $_ && $exonends[$idx] >= $exonhash{$_}) {
						delete($exonhash{$_});
						$exonhash{$exonstarts[$idx]} = $exonends[$idx];
						$hit++;
					}
					## this exon extends the one in the hash 3'
					elsif ($exonstarts[$idx] >= $_ && $exonstarts[$idx] <= $exonhash{$_} && $exonends[$idx] > $exonhash{$_}) {
						$exonhash{$_} = $exonends[$idx];
						$hit++;
					}
					## this exon extends the one in the hash 5'
					elsif ($exonstarts[$idx] <= $_ && $exonends[$idx] <= $exonhash{$_} && $exonends[$idx] >= $_) {
						my $e = $exonhash{$_};
						delete($exonhash{$_});

						$exonhash{$exonstarts[$idx]} = $e;#xonhash{$_};
						$hit++;
					}
				}
				if ($hit == 0) {
					 $exonhash{$exonstarts[$idx]} = $exonends[$idx];
				}
 
				$idx++;
			}
			$entries = 1;
			next;
		}
		## 3. same/no? symbol, first item seen (laststop = 0)
		if ($start >= $laststop && $laststop == 0) {
			$firststart = $start;
			$laststop = $stop;
			$firstcdsstart = $cdsstart;
			$lastcdsstop = $cdsstop;
			$entries++;
			my $idx = 0;
			foreach(@exonstarts) {
				#if ($exonhash{$_} < $exonends[$idx] || !defined($exonhash{$_})) {
				#	$exonhash{$_} = $exonends[$idx];
				#}
				$hit = 0;
				foreach(keys(%exonhash)) {
					## this exon replaces the one in the hash.
					if ($exonstarts[$idx] <= $_ && $exonends[$idx] >= $exonhash{$_}) {
						delete($exonhash{$_});
						$exonhash{$exonstarts[$idx]} = $exonends[$idx];
						#delete($exonhash{$_});
						$hit++;
					}

					## this exon extends the one in the hash 3'
					elsif ($exonstarts[$idx] >= $_ && $exonstarts[$idx] <= $exonhash{$_} && $exonends[$idx] > $exonhash{$_}) {
						$exonhash{$_} = $exonends[$idx];
						$hit++;
					}
					## this exon extends the one in the hash 5'
					elsif ($exonstarts[$idx] <= $_ && $exonends[$idx] <= $exonhash{$_} && $exonends[$idx] >= $_) {
						my $e = $exonhash{$_};
						delete($exonhash{$_});
						$exonhash{$exonstarts[$idx]} = $e;
						$hit++;
					}
				} 
				if ($hit == 0) {
					 $exonhash{$exonstarts[$idx]} = $exonends[$idx];
				}

				$idx++;
			}

			next;
		}
		## 4. extension of same symbol by next transcript variant : start <= laststop (fetched in ascending order)
		## check if cdsstart < lowest recorded value
		if ($cdsstart < $firstcdsstart) {
			$firstcdsstart = $cdsstart;
		}
		## check if cdsstop > highest recorded value
		if ($cdsstop > $lastcdsstop) {
			$lastcdsstop = $cdsstop;
		}
		## check if stop > highest recorded value 
		if ($stop > $laststop) {
			$laststop = $stop;
			$entries++;
		}
		my $idx = 0;
		foreach(@exonstarts) {
			## extend exons if recorded end value for startvalue < new end value for this start value
			#if ($exonhash{$_} < $exonends[$idx] || !defined($exonhash{$_})) {
			#	$exonhash{$_} = $exonends[$idx];
			#}
			$hit = 0;
			foreach(keys(%exonhash)) {
				## this exon replaces the one in the hash.
				if ($exonstarts[$idx] <= $_ && $exonends[$idx] >= $exonhash{$_}) {
					delete($exonhash{$_});
					$exonhash{$exonstarts[$idx]} = $exonends[$idx];
					$hit++;
				}
				## this exon extends the one in the hash 3'
				elsif ($exonstarts[$idx] >= $_ && $exonstarts[$idx] <= $exonhash{$_} && $exonends[$idx] > $exonhash{$_}) {
					$exonhash{$_} = $exonends[$idx];
					$hit++;
				}
				## this exon extends the one in the hash 5'
				elsif ($exonstarts[$idx] <= $_ && $exonends[$idx] <= $exonhash{$_} && $exonends[$idx] >= $_) {
					my $e = $exonhash{$_};
					delete($exonhash{$_});
					$exonhash{$exonstarts[$idx]} = $e;#;$exonhash{$_};
					$hit++;
				}
			} 
			if ($hit == 0) {
				 $exonhash{$exonstarts[$idx]} = $exonends[$idx];
			}
				$idx++;
		}
		#next;
		
		$entries++;
	}
	if ($prevsymb ne '') { #last entry
		my $exstartstring = '';
		my $exendstring = '';
		foreach my $key (sort keys %exonhash) {
			$exstartstring .= "$key,";
			$exendstring .= $exonhash{$key} . ",";
		}
		$exstartstring = substr($exstartstring,0,-1);
		$exendstring = substr($exendstring,0,-1);
		my $syns = $InfoByGeneID{$prevgeneid}{'synonym'};
		my $name = $InfoByGeneID{$prevgeneid}{'name'};
		my $omim = $InfoByGeneID{$prevgeneid}{'omimID'};
		
		$sth->execute($prevchr, $firststart, $laststop, $prevstrand, $prevsymb, $exstartstring,$exendstring,$prevgeneid,$name,$syns,$omim,$prevegid,$firstcdsstart,$lastcdsstop, $morbidtxt);
	}
	$sth->finish();
	$sumsth->finish();
	
	# ATTACH MORBID IDs AND PHENOs
        system("cp /tmp/OmimMorbidMap.txt.bu /tmp/OmimMorbidMap.txt ");
	open IN, '/tmp/OmimMorbidMap.txt' or return('could not open morbid file');;
    $query = "UPDATE genesum SET morbidTXT = CONCAT_WS('|',morbidTXT,?), morbidID = omimID WHERE omimID = ?";
	$sth = $dbh->prepare($query);
	$sth2 = $dbh->prepare("UPDATE genes SET morbidID = omimID WHERE omimID = ?");
	my %updated;
	while (<IN>) {
		my $line = $_;
		chomp($line);
		if ($line =~ m/#/) {
			next;
		}
		my @data = split(/\t/, $line);
		my ($phenotype, $appendix);
        # 4 datafields
		my ($description, $genestring, $omimID, $cyto) = @data;
		if ($omimID eq '') {
			#skip if no omimID present
			next;
		}
		## parse the phenotype description and write out phenotype with tags and #morbidID
		if ($description =~ /^(.*?)(, )?(\d{6})?( )?(\((\d)\))?$/) {
			$phenotype = $1;
			my $morbidid = $3;
			#my $origin = $6;

			if (!$morbidid) {
				$morbidid = $omimID;
			}

			if ($phenotype =~ /^{(.*)}$/) {
				$phenotype = $1;
				$appendix = " [suspectibility factor]";
				if ($phenotype =~ /\?(.*)/) {
					$phenotype = $1;
					$appendix .= " [unconfirmed or possibly spurious mapping]";
				}
				$phenotype .= $appendix;
				$phenotype .= " #$morbidid";
			}

			elsif ($phenotype =~ /^\[(.*)\]$/) {
				$phenotype = $1;
				$appendix = " [nondiseases]";
				if ($phenotype =~ /\?(.*)/) {
					$phenotype = $1;
					$appendix .= " [unconfirmed or possibly spurious mapping]";
				}
				$phenotype .= $appendix;
				$phenotype .= " #$morbidid";
			}

			elsif ($phenotype =~ /\?(.*)/) {
				$phenotype = $1;
				$appendix = " [unconfirmed or possibly spurious mapping]";
				if ($phenotype =~ /^\[(.*)\]$/) {
					$phenotype = $1;
					$appendix .= " [nondiseases]";
				}
				elsif ($phenotype =~ /^{(.*)}$/) {
					$phenotype = $1;
					$appendix .= " [suspectibility factor]";
				}
				$phenotype .= $appendix;
				$phenotype .= " #$morbidid";
			}

			else {
				$phenotype .= " #$morbidid";
			}

		}

		else {
			print "NO MATCH: '$description'\n";
		}

		# skip if already seen omim_ID <=> Pheno link
		if ($updated{$omimID}{$phenotype} == 1) {
			next;
		}
		# then store this update
		$updated{$omimID}{$phenotype} = 1;

		# update db based on omimID.
		$sth->execute($phenotype,$omimID);
		$sth2->execute($omimID);
		next;
	}
	close IN;

	# INSERT PUBMED IDS
	print "\t=> Inserting PubMed identifiers\n";
	open IN, '/tmp/gene2pubmed.txt';
	my $psth = $dbh->prepare("UPDATE genesum SET pubmed = CONCAT_WS('|', pubmed, ?) WHERE geneID = ?");
	while (<IN>) {
		chomp($_);
		my @pieces = split(/\t/,$_);
		if ($pieces[0] == 9606) {   # 9606 is taxonomy id for homo sapiens !
			my $geneid = $pieces[1];
			my $pubmedid = $pieces[2];
			$psth->execute($pubmedid,$geneid);
		}
	}
	$psth->finish();
	close IN;

	print "\t=> Done.\n";
	#unlink('/tmp/OmimMorbidMap.txt');
	unlink('/tmp/gene2pubmed.txt');
}
	

