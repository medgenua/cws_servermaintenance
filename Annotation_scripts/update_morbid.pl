#!/usr/bin/perl

use Getopt::Std;
use DBI;
use POSIX;
use LWP;
use HTTP::Request;
use XML::Simple;
use Data::Dumper;

$| = 1;

# read credentials
use Cwd 'abs_path';
$credpath = abs_path("../LoadCredentials.pl");
require($credpath);


##########################
# COMMAND LINE ARGUMENTS #
##########################
# D = Use specific database (mandatory as target db if L is specified), otherwise use default.
my %opts;
getopts('D:', \%opts);  # option are in %opts


# print what will happen
print "#######################\n";
print "# UPDATING OMIM TABLE #\n";
print "#######################\n";
print "\n";


###################################
# CONNECT TO CNVanalysis DATABASE #
###################################
print "1/ Connecting to the database\n";

my $db;
if ($opts{'D'}){
	$db = "CNVanalysis".$opts{'D'};
	print "\t=> Using user specified database $db\n";
}

else {
	# connect to GenomicBuilds DB to get the current Genomic Build Database.
	my $dbhgb = DBI->connect("dbi:mysql:GenomicBuilds:$host",$userid,$userpass);
	my $gbsth =$dbhgb->prepare("SELECT name, StringName FROM CurrentBuild");
	$gbsth->execute();
	my ($ucscbuild, $newbuild) = $gbsth->fetchrow_array();
	$gbsth->finish();

	$db = "CNVanalysis".$ucscbuild;
	print "\t=> Using current build database (build ".$newbuild.")\n";
}

my $connectionInfocnv = "dbi:mysql:$db:$host";
my $dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;



## Get API URL from databases
my $apisth = $dbh->prepare("SELECT url FROM Administration WHERE UpdateScript = 'OMIM_API'");
$apisth->execute();
my @array = $apisth->fetchrow_array();
my $apiurl = $array[0];


print "2/ Downloading the OMIM MORBID datafile\n";
##########################
# GET UPDATED GENE LISTS #
##########################
my $urlsth = $dbh->prepare("SELECT item, url FROM Administration WHERE UpdateScript = 'update_morbid.pl'");
my $urlrows = $urlsth->execute();
my $failed = 0;
if ($urlrows == 0) {
	print "\t=> No URLs specified\n";
	$failed++;
}
#print "rows found: $urlrows\n";
while (my @array = $urlsth->fetchrow_array()) {
	my $url = $array[1];
	my $item = $array[0];
	if ($url eq 'LIFT' || $url eq '') {
		print " NO URL ('lift')\n";
		$failed = 1;
		next;
	}
	print "\t$item from $url : ";
	if ($url !~ m/\.gz$/i) {
		`wget --no-check-certificate -q -O '/tmp/$item.txt' '$url' && chmod 777 '/tmp/$item.txt'`;
	}
	else {
		`wget --no-check-certificate -q -O '/tmp/$item.txt.gz' '$url' && gunzip '/tmp/$item.txt.gz' --force && chmod 777 '/tmp/$item.txt'`;
	}
	if (-z "/tmp/$item.txt" || -e "/tmp/$item.txt.gz") {
		print " FAILED\n";
		$failed = 1;
	}
	else {
		print " OK\n";
	}
}
if ($failed == 1) {
	print "\n";
	print "\t####################################################\n";
	print "\t# SOME FILES FAILED TO DOWNLOAD, CHECK YOUR URLs ! #\n";
	print "\t####################################################\n";
	print "\n";
	print "\t################\n";
	print "\t# NOT UPDATING #\n";
	print "\t################\n";
	die("SOME FILES FAILED TO DOWNLOAD, CHECK YOUR URLs !");
}

print "\t=> All files successfully downloaded.\n";

########################################
# EVERYTHING DOWNLOADED, START PARSING #
########################################
print "3/ Updating the MORBID table\n";

## First remove old OMIM data
my $trunc = "TRUNCATE TABLE morbidmap";
$dbh->do($trunc);


my $query = "INSERT INTO morbidmap (`morbidID`, `omimID`, `disorder`, `inheritance mode`, `locus`, `phenotype mapping key`, `suspectibility factor`, `non-disease`, `provisional`) values (?, ?, ?, ?, ?, ?, ?, ?, ?)";
my $sth =  $dbh->prepare($query);

# get number of lines
my $lines = `wc -l /tmp/OmimMorbidMap.txt`;
chomp($lines);
print "\t=> $lines MORBID entries, processing...\n";


open IN, '/tmp/OmimMorbidMap.txt' or return('could not open morbid file');;
my $percblock = 10;
my $milestones = ceil($lines/100*$percblock);    # adjust as needed
my $counter = 0;
while (<IN>) {
	show_progress($counter/$milestones, $percblock) if (++$counter % $milestones == 0);
	## skip comments
	next if $_ =~ /^#/;
	chomp;

	my ($nondisease, $multifactoral, $provisional);
	my ($phenotype, $morbidid, $evidence, $inheritance);
	my ($phenotext, $genesymbols, $mim, $cytolocation) = split(/\t/, $_);

	## get MorbidID (get first 6-digit ID)
	if ($phenotext =~ /(\d{6})/) {
		$morbidid = $1;
	}
	else {
		$morbidid = $mim;
	}
	## make API call to get inheritance, phenotype and evidence
	my $phenomaps = getPhenotypes($mim, $apiurl);
	## if only one phenoMap in results
	if (ref $phenomaps eq ref {}) {
		$inheritance = $phenomaps->{phenotypeInheritance};
		$phenotype = $phenomaps->{phenotype};
		$evidence = $phenomaps->{phenotypeMappingKey};
	}
	else {
		## multiple phenoMaps, find matching phenoMaps
		my @matchingPhenomaps;
		foreach my $phenomap (@{$phenomaps}) {
			my $phenoMim;
			if ($phenomap->{phenotypeMimNumber}) {
				$phenoMim = $phenomap->{phenotypeMimNumber};
			}
			else {
				$phenoMim = $phenomap->{mimNumber};
			}
			if ( $morbidid == $phenoMim ) {
				push(@matchingPhenomaps, $phenomap);
			}
		}

		## only one matching phenoMap
		if (scalar(@matchingPhenomaps) == 1) {
			$inheritance = $matchingPhenomaps[0]->{phenotypeInheritance};
			$phenotype = $matchingPhenomaps[0]->{phenotype};
			$evidence = $matchingPhenomaps[0]->{phenotypeMappingKey};
		}
		## Multiple matching, find best match based on phenotype length match
		else {
			my $matchlength = 0;
			foreach my $match (@matchingPhenomaps) {
				my $testphenotype = $match->{phenotype};
				if ($phenotext =~ /\Q$testphenotype/i) {
					my $currentmatchlength = length($testphenotype);
					## biggest regex hit is the wanted phenotype
					if ($currentmatchlength > $matchlength) {
						$matchlength = $currentmatchlength;
						$inheritance = $match->{phenotypeInheritance};
						$evidence = $match->{phenotypeMappingKey};
						$phenotype = $match->{phenotype};
					}
				}

			}
		}


	}	
	
	if (ref $inheritance eq ref {}) {
		# empty?
		if (%{$inheritance}) {
			die("Died on inheritance:".Dumper($inheritance));
		}
		else {
			$inheritance = undef;
		}
	}

	if ($phenotype=~ /^{(.*)}$/) {
		$phenotype = $1;
		$multifactoral = 1;
	}
	elsif ($phenotype =~ /^\[(.*)\]$/) {
		$phenotype = $1;
		$nondisease = 1;
	}

	if ($phenotype =~ /^\?(.*)/) {
		$phenotype = $1;
		$provisional = 1;
	}


	## all data ready, insert in DB
	$sth->execute($morbidid, $mim, $phenotype, $inheritance, $cytolocation, $evidence, $multifactoral, $nondisease, $provisional);

}

print "\t=> 100%\n";

$sth->finish();

close IN;

print "\n";
print "################\n";
print "# ALL FINISHED #\n";
print "################\n";

sub getPhenotypes {
	# search on gene OMIM ID via gene map, then grab the phenotype map corresponding with the Morbid ID
	my ($geneid, $api) = @_;

    ## use curl, ssl issues.

	## prepare API call
	my $browser = LWP::UserAgent->new;
	my $url = $api.$geneid;
    my $xml_string = `curl -ksS '$url'`;
	# without certificate check
	#$browser->ssl_opts(verify_hostname => 0);
	#my $response = $browser->get($url, 'User-Agent' => 'Mozilla/4.0 (compatible; MSIE 7.0)',);
	my $xml = XML::Simple->new;
	#my $output = $xml->XMLin($response->content);
    my $output = $xml->XMLin($xml_string);

	return($output->{listResponse}->{geneMapList}->{geneMap}->{phenotypeMapList}->{phenotypeMap});

}

sub show_progress {
	my ($milestone, $unit) = @_;
	my $completed = $milestone * $unit;
	print "\t=> $completed%\n";
}
