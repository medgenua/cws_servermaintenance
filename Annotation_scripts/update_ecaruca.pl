#!/usr/bin/perl
use Getopt::Std;
use DBI;

# read credentials
use Cwd 'abs_path';
$credpath = abs_path("../LoadCredentials.pl");
require($credpath);

$| = 1;

# DEFINE VARIABLES
my %chromhash = ();
my @chroms = (1..22);
foreach (@chroms)  {
	$chromhash{ $_ } = $_;
}
$chromhash{ "23" } = "X";
$chromhash{ "24" } = "Y"; 
$chromhash{ "X" } = "23";
$chromhash{ "Y" } = "24";
$chromhash{ "M" } = "25";
$chromhash{ "25" } = "M";

####################################
## CHROMOSOME SIZES IN BUILD hg17 ##
####################################
## ecaruca.track contains some errors...
$maxsize{"1"} = 245522847;
$maxsize{"2"} = 243018229;
$maxsize{"3"} =199505740 ;
$maxsize{"4"} =191411218 ;
$maxsize{"5"} =180857866 ;
$maxsize{"6"} = 170975699;
$maxsize{"7"} = 158628139;
$maxsize{"8"} = 146274826;
$maxsize{"9"} = 138429268;
$maxsize{"10"} = 135413628;
$maxsize{"11"} = 134452384;
$maxsize{"12"} = 132449811;
$maxsize{"13"} = 114142980;
$maxsize{"14"} = 106368585;
$maxsize{"15"} = 100338915;
$maxsize{"16"} = 88827254;
$maxsize{"17"} = 78774742;
$maxsize{"18"} = 76117153;
$maxsize{"19"} = 63811651;
$maxsize{"20"} = 62435964;
$maxsize{"21"} = 46944323;
$maxsize{"22"} = 49554710;
$maxsize{"23"} = 154824264;
$maxsize{"X"} = 154824264;
$maxsize{"24"} = 57701691;
$maxsize{"Y"} = 57701691;
#$maxsize{"25"} = ;

##########################
# COMMAND LINE ARGUMENTS #
##########################
# L = Looking at a new database, LiftOver old values if needed
# D = Use specific database (mandatory if L is specified), otherwise use default.
# d = Database to use as source (mandatory if L is specified.
# u = User ID, mandatory


getopts('LD:d:u:', \%opts);  # option are in %opts

# print name of script and what will happen
print "#####################################\n";
print "# UPDATING ECARUCA ANNOTATION TABLE #\n";
print "#####################################\n";
print "\n";


################################### 
# CONNECT TO CNVanalysis DATABASE #
###################################
my $db = "";

print "1/ Connecting to the database\n";

if ($opts{'D'}){
	$db = "CNVanalysis".$opts{'D'};
	$targetdb = $opts{'D'};
	print "\t=> Using user specified database $db\n";
}
else {
	# connect to GenomicBuilds DB to get the current Genomic Build Database. 
	my $dbhgb = DBI->connect("dbi:mysql:GenomicBuilds:$host",$userid,$userpass);
	my $gbsth =$dbhgb->prepare("SELECT name, StringName FROM CurrentBuild");
	$gbsth->execute();
	my @row = $gbsth->fetchrow_array();
	$db = "CNVanalysis".$row[0];
	$newbuild = $row[1];
	$targetdb = $row[0];
	print "\t=> Using current build database (build ".$row[1].")\n";	
}
$connectionInfocnv="dbi:mysql:$db:$host"; 
$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
$sourcedb = $opts{'d'};
if ($opts{'u'}) {
	$uid = $opts{'u'};
}
else {
	$uid = 1;
}

## CHECK FOR CHAINFILE
my $chainfile;
my $query = "SELECT url, filename FROM `GenomicBuilds`.`LiftFiles` WHERE frombuild = '-hg17' AND tobuild = '".$opts{'D'}."'";
my $sth = $dbh->prepare($query);
my $rows = $sth->execute();
#if ($rows == 0) {	
#	die('Chain File is is not specified and not present on the system');
#}
my @row = $sth->fetchrow_array();
my $url = $row[0];
my $filename = $row[1];
if (-s "$filename" && $filename ne '') {
	$chainfile = $filename;
	print "Found ChainFile in the database ($chainfile)\n";
}
elsif ($url ne '' && $filename ne '') {
	if (-e "$filename" ) {
		unlink("$filename");
	}
	print "\t=> Need to fetch the chainfile: ";
	system("wget -q -O '$filename.gz' '$url' && gunzip '$filename.gz' --force");
	if (-z "$filename" || -e "$filename.gz") {
		die('Chain File Could not be fetched from the internet');
	}
	print "OK\n";
	$chainfile = $filename;
}
else {
	# risky, try to compose url and filename ourselves.
	print "\t=> Need to search for the chainfile online (from ad hoc url): ";
	my $tempsource = '-hg17';
	$tempsource =~ s/^-//;
	my $temptarget = $targetdb;
	$temptarget =~ s/^-//;
	$temptarget =~ s/^hg/Hg/;
	my $url ="http://hgdownload.cse.ucsc.edu/goldenPath/$tempsource/liftOver/$tempsource"."To$temptarget".".over.chain.gz";
	my $tempfile = "$tempsource"."To$temptarget".".over.chain";
	if (-s "../LiftOver/$tempfile") {
		$chainfile = $tempfile;
		print " OK, file was already present\n";
	}
	else {
		system("cd ../LiftOver && wget -q -O '$tempfile.gz' '$url' && gunzip '$tempfile.gz' --force");
		if (-z "../LiftOver/$tempfile" || -e "../LiftOver/$tempfile.gz") {
			die('Chain File Could not be fetched from the internet');
		}
		print "OK\n";
		$chainfile = $tempfile;
	}
}	


#######################
# FETCH DATA TABLE URL #
########################
my $query = "SELECT url FROM Administration WHERE item = 'ecaruca.track'";
my $sth = $dbh->prepare($query);
$sth->execute();
my @row = $sth->fetchrow_array();
my $url = $row[0];
$sth->finish();

#########################
# GET UPDATED CNV LISTS #
#########################
if (($url eq 'LIFT' || $url eq '') && $opts{'L'}) {
	# no url, and liftOver command specified
	print "2/ LiftOver Build $sourcedb to $targetdb\n"; 
	print "\t##############################\n";
	print "\t# STARTING LIFT OVER PROCESS #\n";
	print "\t##############################\n";
	system("cd ../LiftOver/ && ./LiftOver.pl -U $uid -d '$sourcedb' -D '$targetdb' -T 'ecaruca'");
	print "\n";
	#print "Final Step: Summarise data\n";
	#&Summarise;
	print "###################\n";
	print "# UPDATE FINISHED #\n";
	print "###################\n";
	exit;

}
elsif ($url eq 'LIFT' || $url eq '') {
	# no url, and liftOver command NOT specified, skip update.
	print "2/ No URL available for this annotation track\n";
	print "\t=> Not updating \n";
	print "\n";
	print "###################\n";
	print "# UPDATE FINISHED #\n";
	print "###################\n";
}
else {
	# url available: proceed
	print "2/ Downloading Ecaruca UCSC track file\n";
	print "\t=> From $url: ";
	if (-e '/tmp/ecaruca.txt') {
		unlink('/tmp/ecaruca.txt');
	}

	`wget -q -O /tmp/ecaruca.txt $url `;
	if (-z '/tmp/ecaruca.txt') {
		print "Download Failed ! \n";
		die('');
	}
	print "Done\n";
	system("cd /tmp/ && dos2unix ecaruca.txt");
	# CLEAR THE TABLE
	my $query = "TRUNCATE TABLE ecaruca";
	$sth = $dbh->prepare($query);
	$sth->execute();
	$sth->finish();
	# READ PREVIOUS ECARUCA LIFTS
	$tdb = substr($targetdb,1);
	my $lifts;
	my $regions;
	if (-e  "Ecaruca_Files/update_ecaruca.$tdb.txt") {
		open IN, "/opt/ServerMaintenance/Ecaruca_Files/update_ecaruca.$tdb.txt";
		while ( <IN> ) {
			chomp($_);
			my @p = split(/\t/,$_);
			# hash by case id.
			$lifts{$p[0]}{'chr'} = $p[1];  # chr is integer, without the 'chr' prefix !
			$lifts{$p[0]}{'start'} = $p[2];
			$lifts{$p[0]}{'stop'} = $p[3];
			# hash by region
			$regions{$p[4]}{'chr'} = $p[1];	# region is 'integer:start-stop' syntax, without the 'chr' prefix
			$regions{$p[4]}{'start'} = $p[2];
			$regions{$p[4]}{'stop'} = $p[3];
		}
		close IN;
	}		

	#
	open IN, "/tmp/ecaruca.txt";
	print "3/ Inserting ecaruca annotations :\n";
	open FAIL, ">Ecaruca_Files/update_ecaruca.$tdb.failed.txt";
	$line = <IN>;
	while ($line !~ m/^track/) {
		$line = <IN>;
	}
	while (<IN>) {
		my $line = $_;
		#print "processing: $line";
		chomp($line);
		my @pieces = split(/\t/,$line);
		my $chr = $pieces[0];
		$chr =~ s/chr//;
		my $start = $pieces[1];
		my $stop = $pieces[2];
		my $case = $pieces[3];
		my $newstart;
		my $newstop;
		my $newchr;
		# check chromosome size
		if ($maxsize{$chr} < $stop) {
			#print "Shrinking qter position for case $case\n";
			$stop = $maxsize{$chr};
		}

		# case 1 : same region already lifted
		if (exists($regions{$chr.":".$start."-".$stop})) {
			#print "region $chr:$start-$stop already lifted\n";
			$newchr = $regions{$chr.":".$start."-".$stop}{'chr'};
			$newstart = $regions{$chr.":".$start."-".$stop}{'start'};
			$newstop = $regions{$chr.":".$start."-".$stop}{'stop'};
			#print "region $chr:$start-$stop already lifted for case $case (to $newchr:$newstart-$newstop)\n";
		}	
		# case 2 : need to lift
		else {
			my $infile = "/tmp/LiftOver_ecaruca.region.in";
			my $outOK = "/tmp/LiftOver_ecaruca.region.outOK";
			my $outBAD = "/tmp/LiftOver_ecaruca.region.outBAD";
			my $outMerged = "/tmp/LiftOver_ecaruca.region.outMerged";
			open OUT, ">$infile" ;
			print OUT "chr$chr\t$start\t$stop\t$case\n";
			close OUT;
			## first single region lift
			system("cd ../LiftOver/ && ./liftOver -minMatch=0.8 '$infile' '$chainfile' '$outOK' '$outBAD' > /dev/null 2>&1");
			if (-z $outOK) {
				# single region lift failed
				#print "case $case , single lift failed\n";
				unlink($outOK);
				unlink($outBAD);
				# try multi-region-lift
				system("cd ../LiftOver/ && ./liftOver -minMatch=0.8 -multiple '$infile' '$chainfile' '$outOK' '$outBAD' > /dev/null 2>&1");
				if (-z $outOK) {
					# lift failed
					my $reason = `head -n 1 $outBAD`;
					chomp($reason);
					$reason =~ s/#//;
					$reason = "LiftOver Failed: $reason";
					print FAIL "case $case : chr$chr:$start-$stop : $reason\n";
					#print "chr$chr:$start-$stop : $reason\n";
					next;
					#return (0,$reason,'','','');
				}
				else {
					# lift ok
					system("cd ../LiftOver/ && ./liftOverMerge -mergeGap=10000 '$outOK' '$outMerged' && mv '$outMerged' '$outOK' > /dev/null 2>&1");
					open RES, "$outOK";
					my @lines = <RES>;
					close RES;
					if (scalar(@lines) > 1) {
						# region is split => Failed
						#return(0,'LiftOver Failed: Region is split in new build','','','');
						#print "case $case : chr$chr:$start-$stop : LiftOver Failed: Region is split in new build\n";
						print FAIL "case $case : chr$chr:$start-$stop : LiftOver Failed: Region is split in new build\n";
						next;
					}
					else {
						my $line = $lines[0];
						chomp($line);
						my @parts = split(/\t/,$line);
						$parts[0] =~ s/chr//;
						#print "case $case : chr$chr:$start-$stop : LIFT OK (split & merged) : ";
						$newchr = $parts[0];
						$newstart = $parts[1];
						$newstop = $parts[2];
						#print "chr$newchr:$newstart-$newstop \n";
						$regions{"$chr:$start-$stop"}{'chr'} = $newchr;
						$regions{"$chr:$start-$stop"}{'start'} = $newstart;
						$regions{"$chr:$start-$stop"}{'stop'} = $newstop;
						open OUT, ">>Ecaruca_Files/update_ecaruca.$tdb.txt";
						print OUT "$case\t$newchr\t$newstart\t$newstop\t$chr:$start-$stop\n";
						close OUT;
					}		
				}
			}
			else {
				# single region lift ok
				open RES, "$outOK" ;
				my $inline = <RES>;
				close RES;
				chomp($inline);
				#print "out: $inline\n";
				my @parts = split(/\t/,$inline);
				$parts[0] =~ s/chr//;
				#print "case $case : chr$chr:$start-$stop : LIFT OK  : ";
				$newchr = $parts[0];
				$newstart = $parts[1];
				$newstop = $parts[2];
				#print "chr$newchr:$newstart-$newstop \n";
				$regions{"$chr:$start-$stop"}{'chr'} = $newchr;
				$regions{"$chr:$start-$stop"}{'start'} = $newstart;
				$regions{"$chr:$start-$stop"}{'stop'} = $newstop;
				open OUT, ">>Ecaruca_Files/update_ecaruca.$tdb.txt";
				print OUT "$case\t$newchr\t$newstart\t$newstop\t$chr:$start-$stop\n";
				close OUT;
			}

				
		}
		$newchr = $chromhash{$newchr};
		my $query = "INSERT INTO ecaruca (chr, start, stop, caseid) values ('$newchr', '$newstart', '$newstop', '$case') ";
		$dbh->do($query);
		#$nrgenes++;
	}
	
	close IN;
	unlink('/tmp/ecaruca.txt');
	print "\t=> Done\n";
	print "\n";
	print "###################\n";
	print "# UPDATE FINISHED #\n";
	print "###################\n";
	
}
