#!/usr/bin/perl

################
# LOAD MODULES #
################
use DBI;
use Getopt::Std;

# read credentials
use Cwd 'abs_path';
$credpath = abs_path("../LoadCredentials.pl");
require($credpath);

getopts('D:', \%opts);  # option are in %opts

$|++;
###########################
# CONNECT TO THE DATABASE #
###########################
$db;
if ($opts{'D'}){
	$db = "CNVanalysis".$opts{'D'};
	$targetdb = $opts{'D'};
}
else {
	# connect to GenomicBuilds DB to get the current Genomic Build Database. 
	my $dbhgb = DBI->connect("dbi:mysql:GenomicBuilds:$host",$userid,$userpass);
	my $gbsth =$dbhgb->prepare("SELECT name, StringName FROM CurrentBuild");
	$gbsth->execute();
	my @row = $gbsth->fetchrow_array();
	$db = "CNVanalysis".$row[0];
	$newbuild = $row[1];
	print "Assuming build ".$row[1]."\n";
	$gbsth->finish();
	$dbhgb->disconnect;	
}
$connectionInfocnv="dbi:mysql:$db:$host"; 
$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
$dbh->{mysql_auto_reconnect} = 1;


# UPDATE SIZE VALUES
print "Updating Size entries\n";
my $updatesize = "UPDATE aberration SET size = stop - start WHERE size = '-1' ";
my $sthsize = $dbh->prepare($updatesize);
$sthsize->execute();
$sthsize->finish();

###################################################
# GET NUMBER OF HIGH QUALITY SAMPLES PER CHIPTYPE #
###################################################
my %coltotal = ();
my %chiphash = ();
my $totalnumber = 0;
my %controls = ();
my $chiptypes = "SELECT name, ID FROM chiptypes";
my $sthct = $dbh->prepare($chiptypes);
$sthct->execute();
my $coltotal = 0;
while (@names = $sthct->fetchrow_array()) {
	my $chiptype = $names[0];
	my $chiptypeid = $names[1];
	my $nrcontrols = "SELECT COUNT(s.id) FROM sample AS s JOIN projsamp AS ps JOIN project AS p ON s.id = ps.idsamp AND ps.idproj=p.id WHERE p.chiptype='$chiptype' AND p.collection = 'Controls' AND ps.iniwave = '' AND ps.calc = 1 ";	
	my $sthco = $dbh->prepare($nrcontrols);
	$sthco->execute();
	my @cons = $sthco->fetchrow_array();
	$sthco->finish();
	$controls{ $chiptypeid } = $cons[0];
	$totalnumber += $cons[0];
	print "$chiptype" . " has $cons[0] high quality control samples\n";
}
	$sthct->finish();
print "$totalnumber samples are under consideration in total\n";

###################
# PREPARE QUERIES #
###################
my $insidecurrent = " ( (a.start >= ? AND a.stop <= ?) AND ((a.size) > 0.85*(?)) ) "; 
my $extend5prime = " ( a.start <= ? AND (a.stop BETWEEN ? AND ?) AND (? - a.start) <= 0.05*(a.size) ) ";
my $extend3prime = " ( a.stop >= ? AND (a.start BETWEEN ? AND ?) AND (a.stop - ?) <= 0.05*(a.size) )";
my $extendboth = " ( a.start <= ? AND a.stop >= ? AND ((? - a.start)+(a.stop - ?)) <= 0.05*(a.size) )";

my $from = "aberration AS a JOIN projsamp AS ps JOIN project AS p ON a.sample = ps.idsamp AND ps.idproj = a.idproj AND a.idproj = p.id"; 
my $where = "a.chr = ? AND p.collection = 'Controls' AND p.chiptypeid = ? AND a.cn = ? AND ps.iniwave = '' ";
$where = $where . " AND ( $insidecurrent OR $extend5prime OR $extend3prime OR $extendboth ) ";
my $query3 = "SELECT a.id FROM $from WHERE $where";
my $sth3 = $dbh->prepare($query3);

$where = "a.chr = ? AND p.collection = 'Controls' AND a.cn = ? AND ps.iniwave = '' ";
$where = $where . " AND ( $insidecurrent OR $extend5prime OR $extend3prime OR $extendboth ) ";
my $query4 = "SELECT p.chiptypeid FROM $from WHERE $where";
my $sth4 = $dbh->prepare($query4);

$where2 = "cn = ? ";
$where2 .= " AND ( $insidecurrent OR $extend5prime OR $extend3prime OR $extendboth ) ";
my $query5 = "SELECT a.chiptypeid FROM ConTemp a WHERE $where2";
my $sth5 = $dbh->prepare($query5);
	
my $update = "UPDATE aberration SET seeninchip = ?, seenall = ?, seenincontrols = ? WHERE id = ?";
my $sthi = $dbh->prepare($update);

my $aberrations = "SELECT a.id, a.cn, a.start, a.stop, a.size, p.chiptypeid FROM aberration AS a JOIN project AS p ON a.idproj = p.id WHERE a.chr = ? AND p.collection = 'Controls' ORDER BY p.chiptypeid,a.start,a.stop ";
my $stha = $dbh->prepare($aberrations);

$query = "ALTER TABLE ConTemp DELAY_KEY_WRITE = 1";
$sth = $dbh->prepare($query);
$sth->execute();
$sth->finish();



#######################
# GET THE ABERRATIONS #
#######################


my $overall = 0;
my $inchip = 0;
my $incontrols = 0; 
for ($chr=1;$chr<=23;$chr++) {
	print "Evaluating chromosome $chr\n";
	print "  Loading Temporary Data: ...";
	my $query = "TRUNCATE ConTemp";
	$sth = $dbh->prepare($query);
	$sth->execute();
	$sth->finish();
	$query = "INSERT INTO ConTemp (id, cn, start, stop, size, chiptypeid) SELECT a.id, a.cn, a.start, a.stop, a.size, p.chiptypeid FROM aberration a JOIN project p JOIN projsamp ps ON a.idproj = p.id AND a.sample = ps.idsamp AND ps.idproj = p.id WHERE p.collection = 'Controls' AND ps.iniwave = '' AND a.chr = $chr AND ps.calc = 1";
	$dbh->do($query);
	print " Done\n";
	
	my $prevstart = 0;
	my $prevstop = 0;
	my $rowcache;
	my $rows;
	$rows = $stha->execute($chr);
	print "  $rows rows to update\n";
	if ($rows < 10000) {
		$max = $rows;
	}
	else {
		$max = 10000;
	}
   	while (my $aref = shift(@$rowcache) || shift( @{$rowcache = $stha->fetchall_arrayref(undef,$max)|| []})) {
		my $subrowcache;
		my $id = $aref->[0];
		my $cn = $aref->[1];
		my $start = $aref->[2];
		my $stop = $aref->[3];
		my $size = $aref->[4];
		my $chiptypeid = $aref->[5];
		if ($start == $prevstart && $stop == $prevstop) {
			$sthi->execute($incontrols, $overall, $incontrols, $id);
			next;
		}
		$prevstart = $start;
		$prevstop = $stop;
		my $inchip = 0;

		$subrows = $sth5->execute($cn, $start, $stop, $size, $start, $start, $stop, $start, $stop, $start, $stop, $stop, $start, $stop, $start, $stop);
		my $submax;
		if ($subrows > 10000) {
			$submax = 10000;
		}
		else {
			$submax = $subrows;
		}
		while (my $sref = shift(@$subrowcache) || shift( @{$subrowcache = $sth5->fetchall_arrayref(undef,$submax)|| []})) {
			if ($sref->[0] == $chiptypeid) {
				$incontrols++;
			}
			$overall++;
		}
		if ($controls{ $chiptypeid } > 0) {
			$incontrols = sprintf("%.3f", $incontrols / $controls{ $chiptypeid } );
		}

		$overall = sprintf("%.3f", $overall / $totalnumber );
		# Update the table
		$sthi->execute($incontrols, $overall, $incontrols, $id);
   	}
}
$sthi->finish();
$stha->finish();
$sth3->finish();
$sth4->finish();
