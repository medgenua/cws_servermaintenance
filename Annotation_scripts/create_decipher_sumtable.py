import configparser
from dataclasses import dataclass, field
import os
from pathlib import Path
from typing import List
import sys

from CMG.DataBase import MySQL, MySqlError


@dataclass
class Region:
    chr: int
    start: int 
    stop: int
    types: List = field(default_factory=list)


    def overlap(self, region):
        # new region start before self
        if not self.same_chromosome(region):
            return False
        if region.stop < self.start:
            return False
        if region.start > self.stop:
            return False
        return True
    
    def union(self, region):
        # get outer coordinates to merge 2 regions
        if self.overlap(region):
            coord = [self.start, self.stop, region.start, region.stop]
            coord.sort()
            return coord[0], coord[-1]
            
    def same_chromosome(self, region):
        if region.chr == self.chr:
            return True
        else:
            return False

    def merge(self, region):
        # make a Region object of both Regions objects
        self.start, self.stop = self.union(region)
        for cnvtype, count in region.types.items():
            if cnvtype in self.types:
                self.types[cnvtype] = count + self.types[cnvtype]
            else:
                self.types[cnvtype] = count
        return self
 
"""
Get credentials
"""
basedir = Path(__file__).absolute().parent.parent.parent
default_credentials = os.path.join(basedir, '.Credentials', '.credentials')
if len(sys.argv) > 1:
    credentialsfile = Path(sys.argv[1]).absolute()
else:
    credentialsfile = default_credentials

try:
    # workaround for sectionless config file
    with open(credentialsfile) as f:
        config_string = '[dummy_section]\n' + f.read()
    config = configparser.ConfigParser()
    config.read_string(config_string)
    config = config['dummy_section']
except IOError:
    sys.exit(
        "Could not open credentials file\nUSAGE: \"perl ApplyUpdates.pl /path/to/credentialsfile\" "
        "or make sure %s is present" % default_credentials)

dbuser = config['DBUSER']
dbpass = config['DBPASS']
dbhost = config['DBHOST']
    
"""
Connect to DB
"""
dbh = MySQL(dbuser, dbpass, dbhost, 'GenomicBuilds')
build = dbh.runQuery("SELECT name, StringName FROM CurrentBuild")

if build:
    ucscbuild = build[0]['name']
    newbuild = build[0]['StringName']
    db = "CNVanalysis" + ucscbuild
    print(f"Current build database (build {newbuild})")
else:
    sys.exit("No genomic build found")

dbh.select_db(db)    
    
    
collapsed_regions = list()
 
"""
Get DECIPHER patients data
"""
dbcount = 0
decpatients = dbh.runQuery("SELECT chr, start, stop, type, inheritance FROM decipher_patients WHERE chr != 0 ORDER BY chr, start")
for index, patient in enumerate(decpatients):
    dbcount += 1
    cnvtype = patient['type']
    inheritance = patient['inheritance']
    inheritance = inheritance.replace(' ', '_')
    cnvtype = cnvtype.replace(' ', '_')
    cnvtype = '-'.join([cnvtype, inheritance])
    typedict = {cnvtype : 1}
    cnv = Region(patient['chr'], patient['start'], patient['stop'], typedict)
    
    # start
    if index == 0:
        collapsed_regions.append(cnv)
        continue  
    
    # check if the current region overlaps with an existing one. If so, merge them 
    for region in collapsed_regions:
        overlapped = False
        if region.overlap(cnv):
            region = region.merge(cnv)
            overlapped = True
            break #not needed, the regions don't overlap, just a speed up
    
    # add the new region
    if not overlapped:
        collapsed_regions.append(cnv)
 


"""
Fill in decipher_patsum table
"""
dbh.doQuery("TRUNCATE TABLE `decipher_patsum`")

for region in collapsed_regions:
    concats = list()
    for cnvtype, count in region.types.items():
        concats.append("=".join([cnvtype,str(count)]))
    data = ';'.join(concats)
    dbh.insertQuery(f"INSERT INTO `decipher_patsum` (chr, start, stop, data) VALUES ({region.chr}, {region.start}, {region.stop}, '{data}') ")