#!/usr/bin/perl

use DBI;

# read credentials
use Cwd 'abs_path';
$credpath = abs_path("../LoadCredentials.pl");
require($credpath);

########################################
# CONNECT TO DATABASE AND CLEAR TABLES #
########################################
print "Connecting to Database:\n";
$db = "CNVanalysis";

# connect to GenomicBuilds DB to get the current Genomic Build Database. 
my $dbhgb = DBI->connect("dbi:mysql:GenomicBuilds:$host",$userid,$userpass);
my $gbsth =$dbhgb->prepare("SELECT name, StringName FROM CurrentBuild");
$gbsth->execute();
my @row = $gbsth->fetchrow_array();
$gbsth->finish();
$db = "CNVanalysis".$row[0];
$newbuild = $row[1];
$ucscbuild = $row[0];
$ucscbuild =~ s/^-//;
print "\t=> Using current build database (build ".$row[1].")\n";
$connectionInfo = "dbi:mysql:$db:$host;mysql_local_infile=1";
$dbh = DBI->connect($connectionInfo, $userid, $userpass);

$|++;
my %chromhash = ();
my @chroms = (1..22);
foreach (@chroms)  {
	$chromhash{ $_ } = $_;
}
$chromhash{ "X" } = 23;
$chromhash{ "Y" } = 24; 
$chromhash{ 23 } = "X";
$chromhash{ 24 } = "Y";


# FILL CHIPTYPE / PROJECT LINKS 
print "Fetching control projects for each chiptype: \n";
my %projects;
my @chiptypes = ();

$sth = $dbh->prepare("SELECT ID, Controles,name FROM chiptypes ORDER BY ID");
$sth->execute();
while( my ($id, $con,$name) = $sth->fetchrow_array()) {
	if ($con eq '') {
		print "\t$name (id : $id) : No Control projects specified.\n";
		## no controle projects specified
		next;
	}
	print "\t$name (id : $id) : Control Project ids: $con\n";
	push(@chiptypes,$id);
	$projects{$id}=$con;
}
$sth->finish();

# construct combinations
my $iter = combo(@chiptypes);
####################
# START PROCESSING #
####################
open OUT, ">/tmp/create_con_sumtable.output.txt";
$id = 0;
print "Processing data\n"; 
while (my @combo = $iter->()) {    # Per Combination of chiptypes
   my $instring = '';
   my $chipstring = join('-',@combo);
   foreach (@combo) {
 	my $pid = $projects{$_};
	$instring .= "$pid,";
   }
   $instring = substr($instring,0,-1);
   my $i;
   for ($i = 0; $i<=4; $i++) {	   # Per Copy number
	my $outstring = '';
	print "analysing copy number $i on chiptypes $chipstring : ";
	my $query = "SELECT a.chr, a.start, a.stop, ps.INIWAVE FROM aberration a JOIN projsamp ps ON a.idproj = ps.idproj AND a.sample = ps.idsamp WHERE a.sample = ps.idsamp AND a.cn = '$i' AND a.idproj IN ($instring) AND ps.calc = 1 AND ps.INIWAVE = 0 AND ps.callrate > 0.994 AND ps.LRRSD < 0.2 ORDER BY chr, start,stop ";
	$sth = $dbh->prepare($query);
	$prevstart = 0;
	$prevstop = 0;
	$cstart = 0;
	$cstop = 0;
	$cchr = 1;
	my $rowcache;
	my $rows;
	$rows = $sth->execute();
	
	if ($rows == 0) {
		print "No high quality entries found\n";
		next;	
	}
	print "$rows entries found ";
	if ($rows < 25000) {
		$max = $rows;
	}
	else {
		$max = 25000;
	}

	while (my $row = shift(@$rowcache) || shift( @{$rowcache = $sth->fetchall_arrayref(undef,$max)|| []})) {
		my $chr = $row->[0];
		my $chrtxt = $chromhash{ $chr };
		my $start = $row->[1];
		my $stop = $row->[2];
		my $iniwave = $row->[3];
		if ($iniwave != 0) {
			print "this one has INIWAVE !!!!!!!!!\n";
		}
	  	if ($cchr ne $chr) {
			if ($cstop != 0) {
				$id++;
				$outstring .= "$id\t$cchr\t$cstart\t$cstop\t$i\t$chipstring\n";
				#$query = "INSERT INTO consum (chr, start, stop, cn, chips) VALUES ('$cchr', '$cstart', '$cstop', '$i', '$chipstring')";
				#$dbh->do($query);
			}
			$cstart = 0;
			$cstop = 0;	
			$cchr = $chr;
		}
		if ($cstop == 0) {
			$cstart = $start;
			$cstop = $stop;
			$entries = 1;
		}
		elsif ($start > $cstop ) {
			$id++;
			$outstring .= "$id\t$cchr\t$cstart\t$cstop\t$i\t$chipstring\n";
			#$query = "INSERT INTO consum (chr, start, stop, cn, chips ) VALUES ('$cchr', '$cstart', '$cstop', '$i', '$chipstring')";
			#$dbh->do($query);
			$string = "";
			$cstart = $start;
			$cstop = $stop;
			$entries = 1;
		}
		elsif ($stop > $cstop) {
			$cstop = $stop;
			$entries ++;
		}
		else {
			$entries++;
		}
	}
	print "\n";
	$id++;
	$outstring .= "$id\t$cchr\t$cstart\t$cstop\t$i\t$chipstring\n";
	print OUT $outstring;
	$outstring = '';
	#$query = "INSERT INTO consum (chr, start, stop, cn, chips) VALUES ('$cchr', '$cstart', '$cstop', '$i', '$chipstring')";
	#$dbh->do($query);
    }
    if (@combo == @chiptypes) {
	last;
    }

} 
########################
## LOAD DATA TO TABLE ##
########################
close OUT;
my $trunc = "TRUNCATE TABLE consum";
$dbh->do($trunc);
## for this to work, make sure the option 'local-infile=1' is set on the MYSQL client in the [mysql] section and the MySQL server in the [mysqld] section in the config file (default my.cnf), else this gives an error
$dbh->do("LOAD DATA LOCAL INFILE '/tmp/create_con_sumtable.output.txt' INTO TABLE consum");
system("rm -f /tmp/create_con_sumtable.output.txt");

#------------------------------------------------------------
# Return an iterator of all possible combinations (of all
# lengths) of a set of symbols with the constraint that each
# symbol in each result is less than the symbol to its right.
#
sub combo {
    # The symbols we draw our results from:
    my @list = @_;
    # The trivial case
    return sub { ( ) } if ! @_;

    # Persistent state for the closure
    my (@position, # Last set of symbol indices generated
        @stop);    # Last set possible for $by symbols

    # Start by telling iterator that it just finished
    # (next=1) all results of 0 digits.
    my ($by, $next) = (0, 1);

    return sub {
    if ( $next ) {
            # We finished all combos of size $by, now do $by+1
            $by++;
	    # Start with leftmost $by symbols (except last,
            # which is preincremented before use)
            @position = (0 .. $by - 2, $by - 2);

            # Our stop condition is when we've returned the
            # rightmost $by symbols
            @stop = @list - $by .. $#list;

            $next = undef;
        }

        # Start by trying to advance the rightmost digit
        my $cur = $#position;
        {   # **** redo comes back here! ****

            # Advance current digit to next symbol
            if ( ++$position[ $cur ] > $stop[ $cur ] ) {

                # Keep trying next-most rightmost digit
                # until we find one that's not 'stopped'
		#last if ($cur < 0) ;
               	 	$position[ --$cur ]++;
               	 	redo if $position[ $cur ] > $stop[ $cur ];
		
                	# Reset digits to right of current digit to
                	# the leftmost possible positions
                	my $new_pos = $position[ $cur ];
                	@position[$cur .. $#position] = $new_pos .. $new_pos+$by;
		#}
		#else {
			#return sub { ( ) };
		#}
            }
        }

        # Advance to next result size when we return last
        # possible result of this size
        $next = $position[0]==$stop[0];

        return @list[ @position ];
    }
}

