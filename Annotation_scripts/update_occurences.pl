#!/usr/bin/perl

################
# LOAD MODULES #
################
use DBI;
use Getopt::Std;

# read credentials
use Cwd 'abs_path';
$credpath = abs_path("../LoadCredentials.pl");
require($credpath);

getopts('D:', \%opts);  # option are in %opts
$|++;

###########################
# CONNECT TO THE DATABASE #
###########################
my $db;

if ($opts{'D'}){
	$db = "CNVanalysis".$opts{'D'};
	$targetdb = $opts{'D'};
}
else {
	# connect to GenomicBuilds DB to get the current Genomic Build Database. 
	my $dbhgb = DBI->connect("dbi:mysql:GenomicBuilds:$host",$userid,$userpass);
	my $gbsth =$dbhgb->prepare("SELECT name, StringName FROM CurrentBuild");
	$gbsth->execute();
	my @row = $gbsth->fetchrow_array();
	$db = "CNVanalysis".$row[0];
	$newbuild = $row[1];
	print "Assuming build ".$row[1]."\n";
	$gbsth->finish();
	$dbhgb->disconnect;	
}
$connectionInfocnv="dbi:mysql:$db:$host"; 
$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
$dbh->{mysql_auto_reconnect} = 1;

# UPDATE SIZE VALUES
print "Updating Size entries\n";
my $updatesize = "UPDATE aberration SET size = (stop - start + 1) WHERE size <= 0 OR size = ''";
my $sthsize = $dbh->prepare($updatesize);
$sthsize->execute();
$sthsize->finish();

# FILL CHIPTYPE / PROJECT LINKS 
my %projects;
my @chiptypes;
$projstring = '';
# 370 Duo
$projects{1}=141;
push(@chiptypes,1);
$projstring .= '141,'; 
# 370 Quad
$projects{2}=157;
push(@chiptypes,2);
$projstring .= '157,';
# Cyto V1
$projects{3}=142;
push(@chiptypes,3); 
$projstring .= '142,';
# 610-Qud
$projects{5}=158;
push(@chiptypes,5); 
$projstring .= '158',
# 660W
$projects{6}=160;
push(@chiptypes,6);
$projstring .= '160,'; 
# 1M-V3
$projects{8}=162;
push(@chiptypes,8); 
$projstring .= '162,';
# Omni1-V1
$projects{9}=161;
push(@chiptypes,9); 
$projstring .= '161,';
# Cyto V2.1
$projects{10}=137;
push(@chiptypes,10); 
$projstring .= '137';
# HumanOmniExpress12v1.0
$projects{11}=229;
push(@chiptypes,11); 
$projstring .= '229';



###################################################
# GET NUMBER OF HIGH QUALITY SAMPLES PER CHIPTYPE #
###################################################
my %coltotal = ();
my %chiphash = ();
my $collectionq = "SELECT p.collection FROM project As p GROUP BY collection";
my $sthcol = $dbh->prepare($collectionq);
$sthcol->execute();
my $totalnumber = 0;
my %controls = ();
my %idhash = ();
while (@collections = $sthcol->fetchrow_array()) {
	my $collection = $collections[0];
	my $chiptypes = "SELECT name, ID FROM chiptypes";
	my $sthct = $dbh->prepare($chiptypes);
	$sthct->execute();
	my $coltotal = 0;
	while (@names = $sthct->fetchrow_array()) {
		my $chiptype = $names[0];
		my $id = $names[1];
		$idhash{$id} = $chiptype;
		my $nrsamples = "SELECT COUNT(s.id) FROM sample AS s JOIN projsamp AS ps JOIN project AS p ON s.id = ps.idsamp AND ps.idproj=p.id WHERE p.chiptype='$chiptype' AND p.collection = '$collection' AND ps.callrate > 0.99 AND s.intrack = 1 AND ps.iniwave = 0 AND ps.calc = 1"; 
		my $sthns = $dbh->prepare($nrsamples);
		$sthns->execute();
		my @result = $sthns->fetchrow_array();
		$sthns->finish();
		$chiphash{ $chiptype . "_total" } = $chiphash{ $chiptype. "_total"} + $result[0];	
		$chiphash{ $chiptype . "_$collection" } = $result[0];
		$totalnumber = $totalnumber + $result[0];
		$coltotal{ $collection } = $coltotal{ $collection} + $result[0];
		if ($collection eq "Controls") {
			my $nrcontrols = "SELECT COUNT(s.id) FROM sample AS s JOIN projsamp AS ps JOIN project AS p ON s.id = ps.idsamp AND ps.idproj=p.id WHERE p.chiptype='$chiptype' AND p.id IN ($projstring) AND ps.iniwave = 0 AND ps.callrate > 0.994 AND ps.LRRSD < 0.2";	
			my $sthco = $dbh->prepare($nrcontrols);
			$sthco->execute();
			my @cons = $sthco->fetchrow_array();
			$sthco->finish();
			$controls{ $chiptype } = $cons[0];
			$result[0] = $cons[0];
		}
		print "$chiptype" . "_$collection has $result[0] high quality samples\n";
	}
	$sthct->finish();
}
print "$totalnumber samples are under consideration in total\n";
$sthcol->finish();

###################
# PREPARE QUERIES #
###################
my $insidecurrent = " ( (a.start >= ? AND a.stop <= ?) AND ((a.size) > 0.85*(?)) ) "; 
my $extend5prime = " ( a.start <= ? AND (a.stop BETWEEN ? AND ?) AND (? - a.start) <= 0.05*(a.size) ) ";
my $extend3prime = " ( a.stop >= ? AND (a.start BETWEEN ? AND ?) AND (a.stop - ?) <= 0.05*(a.size) )";
my $extendboth = " ( a.start <= ? AND a.stop >= ? AND ((? - a.start)+(a.stop - ?)) <= 0.05*(a.size) )";

# Query for overlap in non-controls
$where = "( $insidecurrent OR $extend5prime OR $extend3prime OR $extendboth ) ";
my $query2 = "SELECT a.chiptypeid,a.collection FROM OccTemp a WHERE a.cn = ? AND $where";
my $sth2 = $dbh->prepare($query2);


# Query for controls
$where = "a.chiptypeid = ? AND a.cn = ? ";
$where = $where . " AND ( $insidecurrent OR $extend5prime OR $extend3prime OR $extendboth ) ";
my $query3 = "SELECT a.id FROM OccTempCon a WHERE $where";
my $sth3 = $dbh->prepare($query3);
	
# update query
my $update = "UPDATE aberration SET seeninchip = ?, seenall = ?, seenincontrols = ?, schipacoll = ? WHERE id = ?";
my $sthi = $dbh->prepare($update);

# get list of aberrations to process
my $aberrations = "SELECT a.id, a.cn, a.start, a.stop, a.size, p.collection, p.chiptypeid FROM aberration AS a JOIN project AS p ON a.idproj = p.id WHERE a.chr = ? AND NOT p.collection = 'Controls' ORDER BY p.chiptypeid,p.collection,a.start,a.stop ";
my $stha = $dbh->prepare($aberrations);


$query = "ALTER TABLE OccTemp DELAY_KEY_WRITE = 1";
$sth = $dbh->prepare($query);
$sth->execute();
$sth->finish();
$query = "ALTER TABLE OccTempCon DELAY_KEY_WRITE = 1";
$sth = $dbh->prepare($query);
$sth->execute();
$sth->finish();



#######################
# GET THE ABERRATIONS #
#######################
for ($chr=1;$chr<=23;$chr++) {
	print "Evaluating chromosome $chr\n";

	# CREATE TEMP REFERENCE TABLES FOR SPEED
	print "  Loading Temporary Data: ...";
	my $query = "TRUNCATE OccTemp";
	$sth = $dbh->prepare($query);
	$sth->execute();
	$sth->finish();
	$query = "INSERT INTO OccTemp (id, cn, start, stop, size, chiptypeid, collection ) SELECT a.id, a.cn, a.start, a.stop,a.size, p.chiptypeid, p.collection FROM aberration a JOIN project p JOIN projsamp ps JOIN sample s ON a.sample = ps.idsamp AND ps.idproj = a.idproj AND a.idproj = p.id AND a.sample = s.id WHERE s.intrack = 1 AND s.trackfromproject = a.idproj AND chr = $chr AND ps.INIWAVE = 0 AND ps.callrate > 0.99 AND NOT p.collection = 'Controls' AND ps.calc = 1";

	$dbh->do($query);
	my $query = "TRUNCATE OccTempCon";
	$sth = $dbh->prepare($query);
	$sth->execute();
	$sth->finish();
	$query = "INSERT INTO OccTempCon (id, cn, start, stop, size, chiptypeid) SELECT a.id, a.cn, a.start, a.stop, a.size, p.chiptypeid FROM aberration a JOIN project p JOIN projsamp ps ON a.idproj = p.id AND a.sample = ps.idsamp AND ps.idproj = p.id WHERE a.idproj IN ($projstring) AND ps.iniwave = 0 AND ps.callrate > 0.994 AND ps.LRRSD < 0.2 AND a.chr = $chr ";

	$dbh->do($query);
	
	print " Done\n";
	
	# LOCAL VARS
	my $rowcache;
	my $overall = 0;
	my $inchipcol = 0;
	my $inchipall = 0;
	my $incontrols = 0; 	
	my $prevstart = 0;
	my $prevstop = 0;
	
	# GET CNVS FOR CURRENT CHR	
	$stha->execute($chr);
   	my $rows = $stha->rows();
	print "  $rows rows to update\n";
   	if ($rows < 10000) {
		$max = $rows;
   	}
   	else {
 		$max = 10000;
 	}
   	while (my $aref = shift(@$rowcache) || shift( @{$rowcache = $stha->fetchall_arrayref(undef,$max)|| []})) {
		my $subrowcache;
		my $id = $aref->[0];
		my $cn = $aref->[1];
		my $start = $aref->[2];
		my $stop = $aref->[3];
		my $size = $aref->[4];
		my $chiptypeid = $aref->[6];
		my $collection = $aref->[5];
		my $chiptype = $idhash{$chiptypeid};
		if ($start == $prevstart && $stop == $prevstop) {
			$sthi->execute($inchipcol, $overall, $incontrols, $inchipall, $id);
			next;
		}
		$prevstart = $start;
		$prevstop = $stop;
		$prevchiptypeid = $chiptypeid;
		$inchipcol = 0;
		$inchipall = 0;
		$overall = 0;
		$incontroles = 0;
		# Non controls from same collection
		$sth2->execute($cn, $start, $stop, $size, $start, $start, $stop, $start, $stop, $start, $stop, $stop, $start, $stop, $start, $stop);
		#$sth2->execute($chr, $collection, $cn, $start, $stop, $size, $start, $start, $stop, $start, $stop, $start, $stop, $stop, $start, $stop, $start, $stop);
		while (my $sref = shift(@$subrowcache) || shift( @{$subrowcache = $sth2->fetchall_arrayref(undef,$sth2->rows()) || []})) {
			if ($sref->[0] == $chiptypeid ) {
				if ($sref->[1] eq $collection) {
					$inchipcol++;
				}
				$inchipall++;
			}
			if ($sref->[1] eq $collection) {
				$overall++;
			}
		}
		if ($chiphash{ $chiptype . "_$collection" } > 0)  {
			$inchipcol = sprintf("%.3f", $inchipcol / $chiphash{ $chiptype . "_$collection" } );
		}
		if ($coltotal{ $collection} > 0) {
			$overall = sprintf("%.3f", $overall / $coltotal{ $collection } );
		}
		if ($chiphash{ $chiptype . "_total"} > 0) {
			$inchipall = sprintf("%.3f", $inchipall / $chiphash{ $chiptype . "_total" });
		}
		# In controls for this chiptype
		$conrows = $sth3->execute($chiptypeid, $cn, $start, $stop, $size, $start, $start, $stop, $start, $stop, $start, $stop, $stop, $start, $stop, $start, $stop);
		if ($controls{ $chiptype } > 0) {
			$incontrols = sprintf("%.3f", $conrows / $controls{ $chiptype } );
		}
		if ($chr == 8 && $start > 42000000 && $stop < 43000000 && $conrow > 0) {
			print "here\n";
		}
		# Update the table
		 
		$sthi->execute($inchipcol, $overall, $incontrols, $inchipall, $id);
   	}

}
$stha->finish();
$sthi->finish();
$sth2->finish();
$sth3->finish();
