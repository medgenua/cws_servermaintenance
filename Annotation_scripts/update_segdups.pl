#!/usr/bin/perl
use Getopt::Std;
use DBI;

# read credentials
use Cwd 'abs_path';
$credpath = abs_path("../LoadCredentials.pl");
require($credpath);

$| = 1;

# DEFINE VARIABLES
my %chromhash = ();
my @chroms = (1..22);
foreach (@chroms)  {
	$chromhash{ $_ } = $_;
}
$chromhash{ "23" } = "X";
$chromhash{ "24" } = "Y"; 
$chromhash{ "X" } = "23";
$chromhash{ "Y" } = "24";
$chromhash{ "M" } = "25";
$chromhash{ "25" } = "M";

##########################
# COMMAND LINE ARGUMENTS #
##########################
# L = Looking at a new database, LiftOver old values if needed
# D = Use specific database (mandatory if L is specified), otherwise use default.
# d = Database to use as source (mandatory if L is specified.
# u = User ID, mandatory


getopts('LD:d:u:', \%opts);  # option are in %opts

# print name of script and what will happen
print "###################################################\n";
print "# UPDATING SEGMENAL DUPLICATIONS ANNOTATION TABLE #\n";
print "###################################################\n";
print "\n";

################################### 
# CONNECT TO CNVanalysis DATABASE #
###################################
my $db = "";

print "1/ Connecting to the database\n";

if ($opts{'D'}){
	$db = "CNVanalysis".$opts{'D'};
	$targetdb = $opts{'D'};
	print "\t=> Using user specified database $db\n";
}
else {
	# connect to GenomicBuilds DB to get the current Genomic Build Database. 
	my $dbhgb = DBI->connect("dbi:mysql:GenomicBuilds:$host",$userid,$userpass);
	my $gbsth =$dbhgb->prepare("SELECT name, StringName FROM CurrentBuild");
	$gbsth->execute();
	my @row = $gbsth->fetchrow_array();
	$db = "CNVanalysis".$row[0];
	$newbuild = $row[1];
	print "\t=> Using current build database (build ".$row[1].")\n";	
}
$connectionInfocnv="dbi:mysql:$db:$host"; 
$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;

if ($opts{'d'}) {
	$sourcedb = $opts{'d'};
}
if ($opts{'u'}) {
	$uid = $opts{'u'};
}
else {
	$uid = 1;
}


########################
# FETCH DATA TABLE URL #
########################
my $query = "SELECT url FROM Administration WHERE item = 'SegmentalDuplications'";
my $sth = $dbh->prepare($query);
$sth->execute();
my @row = $sth->fetchrow_array();
my $url = $row[0];
$sth->finish();

##########################
# GET UPDATED GENE LISTS #
##########################
if (($url eq 'LIFT' || $url eq '') && $opts{'L'}) {
	# no url, and liftOver command specified
	print "2/ LiftOver Build $sourcedb to $targetdb\n"; 
	print "\t##############################\n";
	print "\t# STARTING LIFT OVER PROCESS #\n";
	print "\t##############################\n";
	system("cd LiftOver/ && ./LiftOver.pl -U $uid -d '$sourcedb' -D '$targetdb' -T 'segdup'");
	print "\n";
	print "Final Step: Summarise data\n";
	&Summarise;
	print "###################\n";
	print "# UPDATE FINISHED #\n";
	print "###################\n";
	exit;

}
elsif ($url eq 'LIFT' || $url eq '') {
	# no url, and liftOver command NOT specified, skip update.
	print "2/ No URL available for this annotation track\n";
	print "\t=> Not updating \n";
	print "\n";
	print "###################\n";
	print "# UPDATE FINISHED #\n";
	print "###################\n";
}
else {
	# url available: proceed
	print "2/ Downloading Segmental Duplications table\n";
	print "\t=> From $url: ";
	if (-e '/tmp/segdup.txt') {
		unlink('/tmp/segdup.txt');
	}

	`wget -q -O /tmp/segdup.txt.gz $url && gunzip /tmp/segdup.txt.gz --force `;
	if (-z '/tmp/segdup.txt') {
		print "Download Failed ! \n";
		die('');
	}
	print "Done\n";

	# FILL THE TABLE
	my $query = "TRUNCATE TABLE segdup";
	$sth = $dbh->prepare($query);
	$sth->execute();
	$sth->finish();
	my $query = "TRUNCATE TABLE segdup_sum";
	$sth = $dbh->prepare($query);
	$sth->execute();
	$sth->finish();

	open IN, "/tmp/segdup.txt";
	print "3/ Inserting SegDups annotations :\n";
	while (<IN>) {
	
		my $line = $_;
		chomp($line);
		my @pieces = split(/\t/,$line);
		if ($pieces[1] =~ m/_/  || $pieces[7] =~ m/_/) {
			next;
		}
		else {
			my $chr = $pieces[1];
			my $start = $pieces[2];
			my $stop = $pieces[3];
			my $strand = $pieces[6];
			my $ochr = $pieces[7];
			my $ostart = $pieces[8];
			my $ostop = $pieces[9];
			my $frac = $pieces[26];
			$chr =~ s/chr//;
			$chr = $chromhash{ $chr };	
			$ochr =~ s/chr//;
			$ochr = $chromhash{ $ochr};
			my $query = "INSERT INTO segdup (chr, start, stop, strand, otherchr, otherstart, otherstop, fraction) values ('$chr', '$start', '$stop', '$strand', '$ochr', '$ostart', '$ostop', '$frac') ";
		
			$dbh->do($query);
			$nrgenes++;
		}
	}
	close IN;
	unlink('/tmp/segdup.txt');
	print "\t=> Done\n";
	print "2/ Summarise Data\n";
	&Summarise;
	print "\n";
	print "###################\n";
	print "# UPDATE FINISHED #\n";
	print "###################\n";
	
}


sub Summarise {
	for ($i = 1; $i<=24; $i++) {
	#	print "$i...";
		my $query = "SELECT start, stop FROM segdup WHERE chr = '$i' ORDER BY start, stop";
		$sth = $dbh->prepare($query);
		$prevstart = 0;
		$prevstop = 0;
		$cstart = 0;
		$cstop = 0;
		$cchr = 1;
		my $rowcache;
		my $rows;
		$rows = $sth->execute();
	
		if ($rows == 0) {
		#	print "No entries found\n";
			next;	
		}
		#print "$rows entries found ";
		#print "Chr 1..";
		if ($rows < 10000) {
			$max = $rows;
		}
		else {
			$max = 10000;
		}
	
		while (my $row = shift(@$rowcache) || shift( @{$rowcache = $sth->fetchall_arrayref(undef,$max)|| []})) {
			my $start = $row->[0];
			my $stop = $row->[1];
			#print "$chr:$start-$stop : $var : $type\n";
			if ($cstop == 0) {
				$cstart = $start;
				$cstop = $stop;
			}
			elsif ($start > $cstop ) {
				#print "$cchr:$cstart-$cstop ($entries entries): \n";
				$query = "INSERT INTO segdup_sum (chr, start, stop) VALUES ('$i', '$cstart', '$cstop')";
				$dbh->do($query);
				$cstart = $start;
				$cstop = $stop;
			}
			elsif ($stop > $cstop) {
				$cstop = $stop;
			}
		}
		#print 	"$cchr:$cstart-$cstop ($entries entries) \n";
		$query = "INSERT INTO segdup_sum (chr, start, stop) VALUES ('$i', '$cstart', '$cstop')";
		$dbh->do($query);
	}
   
	print "\t=> Done\n";
}
