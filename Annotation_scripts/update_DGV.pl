#!/usr/bin/perl

$|++;

use Getopt::Std;
use DBI;
# read credentials
use Cwd 'abs_path';
$credpath = abs_path("../LoadCredentials.pl");
require($credpath);

# DEFINE VARIABLES
my %chromhash = ();
my @chroms = (1..22);
foreach (@chroms)  {
	$chromhash{ $_ } = $_;
}
$chromhash{ "23" } = "X";
$chromhash{ "24" } = "Y";
$chromhash{ "X" } = "23";
$chromhash{ "Y" } = "24";
$chromhash{ "M" } = "25";
$chromhash{ "MT" } = "25";
$chromhash{ "25" } = "M";
##########################
# COMMAND LINE ARGUMENTS #
##########################
# L = Looking at a new database, LiftOver old values if needed
# D = Use specific database (mandatory if L is specified), otherwise use default.
# d = Database to use as source (mandatory if L is specified.
# u = User ID, mandatory

my %opts;
getopts('LD:d:u:', \%opts);  # option are in %opts

# print name of script and what will happen
print "#########################################\n";
print "# UPDATING TORONTO DGV ANNOTATION TABLE #\n";
print "#########################################\n";
print "\n";

###################################
# CONNECT TO CNVanalysis DATABASE #
###################################
my $db = "";

print "1/ Connecting to the database\n";

my ($targetdb, $newbuild, $connectionInfocnv, $dbh, $sourcedb, $uid);
if ($opts{'D'}){
	$db = "CNVanalysis".$opts{'D'};
	$targetdb = $opts{'D'};
	print "\t=> Using user specified database $db\n";
}
else {
	# connect to GenomicBuilds DB to get the current Genomic Build Database.
	my $dbhgb = DBI->connect("dbi:mysql:GenomicBuilds:$host",$userid,$userpass);
	my $gbsth =$dbhgb->prepare("SELECT name, StringName FROM CurrentBuild");
	$gbsth->execute();
	my @row = $gbsth->fetchrow_array();
	$db = "CNVanalysis".$row[0];
	$newbuild = $row[1];
	print "\t=> Using current build database (build ".$row[1].")\n";
}
$connectionInfocnv="dbi:mysql:$db:$host;mysql_local_infile=1";
$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
if ($opts{'d'}) {
	$sourcedb = $opts{'d'};
}
if ($opts{'u'}) {
	$uid = $opts{'u'};
}
else {
	$uid = 1;
}

########################
# FETCH DATA TABLE URL #
########################
my $query = "SELECT url FROM Administration WHERE item = 'TorontoDGV'";
my $sth = $dbh->prepare($query);
$sth->execute();
my @row = $sth->fetchrow_array();
my $url = $row[0];
$sth->finish();

####################
# START PROCESSING #
####################
if (($url eq 'LIFT' || $url eq '') && $opts{'L'}) {
	# no url, and liftOver command specified
	print "2/ LiftOver Build $sourcedb to $targetdb\n";
	print "\t##############################\n";
	print "\t# STARTING LIFT OVER PROCESS #\n";
	print "\t##############################\n";
	system("cd LiftOver/ && ./LiftOver.pl -U $uid -d '$sourcedb' -D '$targetdb' -T 'DGV_full'");
	print "\n";
	print "###################\n";
	print "# UPDATE FINISHED #\n";
	print "###################\n";
	exit;

}
elsif ($url eq 'LIFT' || $url eq '') {
	# no url, and liftOver command NOT specified, skip update.
	print "2/ No URL available for this annotation track\n";
	print "\t=> Not updating \n";
	print "\n";
	print "###################\n";
	print "# UPDATE FINISHED #\n";
	print "###################\n";
}
else {
	# url available: proceed
	print "2/ Downloading Toronto DGV table\n";
	print "\t=> From $url: ";
	if (-e '/tmp/toronto.txt') {
		unlink('/tmp/toronto.txt');
    }
	`wget -q -O /tmp/toronto.txt $url `;
	
	my $recheck = 0;
	if (-z '/tmp/toronto.txt') {
		print " File not found, trying to recover\n";
		# file has zero size, probably bad link, try leaving out the date and mention there should be a newer version
		# suppose date part containts of .XXX.dddd. where XXX is three non digits for month and dddd is a year in four digit notation
		$url =~ s/\.[a-z]{3}\.\d{4}\.txt$/\.txt/;
		#`wget -v -O /tmp/toronto.txt $url `;
		`wget -q -O /tmp/toronto.txt $url `;
		$recheck = 1;
	}
	if ($recheck == 1) {
		if (-z '/tmp/toronto.txt') {
			die("\t=> Toronto DGV Data file not found !! check your links from the Administration pages!\n\n\t=>UPDATE FAILED !!!!\n");
		}
		else {
			print "\t#######################################################################################\n";
			print "\t# Toronto DGV Data File adapted and found. This probably means there is a new version!#\n";
			print "\t# You'd better check your links from the Administration pages!                        #\n";
			print "\t#######################################################################################\n";
			print "\t => Download ";
		}
	}
	my $file = '/tmp/toronto.txt';
	print " OK\n";
	################
	# PARSE & LOAD #
	################
    print "3/ parse input data.\n";
    open IN, $file; 
    open OUT, ">/tmp/load.toronto.txt";
    $nrlines = 0;
    # each entry is on multiple gff lines, (start, end, inner/outer), but has all info in each. track & skip.
    my %seen = ();
    while (my $line = <IN>) {
        chomp($line);
        $nrlines += 1;
        # columns
        my @c = split("\t",$line);
        my %info = ();
        # main info is in column 8 as k=v;k=v; ..
        my @p = split(";",$c[8]);
        foreach (@p) {
            my @i = split("=",$_);
            $info{$i[0]} = $i[1];
        }
        if (defined($seen{$info{'ID'}})) {
            next;
        }
        $seen{$info{'ID'}} = 1;
        # clean up chr
        $c[0] =~ s/chr//;
        # 1:6 : id , chr , start, stop, max_start , max_stop
        my $outline = $info{"ID"} . "\t" . $chromhash{$c[0]} . "\t" ;
          $outline .= $info{"inner_start"} . "\t" . $info{"inner_end"} . "\t";
          $outline .= $info{"outer_start"} . "\t" . $info{"outer_end"} . "\t";
        # 7:8 : vtype ; vsubtype
        $outline .= $info{"variant_type"} . "\t" . $info{"variant_sub_type"} . "\t";
        # 9:12 : numbers
        $outline .= $info{"num_samples"} . "\t" . $info{"Number_of_unique_samples_tested"} . "\t" . $info{"num_studies"} . "\t";
        $outline .= $info{"num_variants"} . "\t" . $info{"num_platforms"} . "\t";
        # 13 : frequency 
        if ($info{'Frequency'} =~ m/^100/) {
            $info{'Frequency'} = '100%'
        }
        else {
            $info{'Frequency'} =~ s/\%//;
            $info{'Frequency'} = substr($info{'Frequency'},0,4).'%';
        }
        $outline .= $info{"Frequency"} ."\n";

        print OUT $outline;
        
        

    }
    close IN;
    close OUT;
    # LOAD
    $dbh->do("TRUNCATE TABLE `DGV_gold`");
    $dbh->do("LOAD DATA LOCAL INFILE '/tmp/load.toronto.txt' INTO TABLE `DGV_gold`");
	# clean
    #system("rm -f /tmp/toronto.txt /tmp/load.toronto.txt");
	print "\t=> $nrlines items were stored\n";
	print "\n";
	print "###################\n";
	print "# UPDATE FINISHED #\n";
	print "###################\n";
	#unlink($file);
}
