#!/usr/bin/perl
use Getopt::Std;
use DBI;

# read credentials
use Cwd 'abs_path';
$credpath = abs_path("../LoadCredentials.pl");
require($credpath);

$|++;
# DEFINE VARIABLES
my %chromhash = ();
my @chroms = (1..22);
foreach (@chroms)  {
	$chromhash{ $_ } = $_;
}
$chromhash{ "23" } = "X";
$chromhash{ "24" } = "Y"; 
$chromhash{ "25" } = "M";
$chromhash{ "X" } = "23";
$chromhash{ "Y" } = "24";
$chromhash{ "M" } = "25";

##########################
# COMMAND LINE ARGUMENTS #
##########################
# L = Looking at a new database, LiftOver old values if needed
# D = Use specific database (mandatory if L is specified), otherwise use default.
# d = Database to use as source (mandatory if L is specified.
# u = User ID, mandatory

getopts('LD:d:u:', \%opts);  # option are in %opts

# print name of script and what will happen
print "#############################################\n";
print "# UPDATING Gencode project ANNOTATION TABLE #\n";
print "#############################################\n";
print "\n";

################################### 
# CONNECT TO CNVanalysis DATABASE #
###################################
my $db = "";
print "1/ Connecting to the database\n";

if ($opts{'D'}){
	$db = "CNVanalysis".$opts{'D'};
	$targetdb = $opts{'D'};
	print "\t=> Using user specified database $db\n";
}
else {
	# connect to GenomicBuilds DB to get the current Genomic Build Database. 
	my $dbhgb = DBI->connect("dbi:mysql:GenomicBuilds:$host",$userid,$userpass);
	my $gbsth =$dbhgb->prepare("SELECT name, StringName FROM CurrentBuild");
	$gbsth->execute();
	my @row = $gbsth->fetchrow_array();
	$db = "CNVanalysis".$row[0];
	$newbuild = $row[1];
	print "\t=> Using current build database (build ".$row[1].")\n";	
}
$connectionInfocnv="dbi:mysql:$db:$host"; 
$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;

if ($opts{'d'}) {
	$sourcedb = $opts{'d'};
}
if ($opts{'u'}) {
	$uid = $opts{'u'};
}
else {
	$uid = 1;
}


########################
# FETCH DATA TABLE URL #
########################
print "2/ Fetching the data resources\n";
my $urlsth = $dbh->prepare("SELECT item, url FROM Administration WHERE UpdateScript = 'update_Encode.pl'");
my $urlrows = $urlsth->execute();
my $failed = 0;
my $lifted = 0;
if ($urlrows == 0) {
	print "\t=> No urls found!\n";
	$failed++;
}
my $manual;
my $auto;
my $classes;
while (my @array = $urlsth->fetchrow_array()) {
	my $url = $array[1];
	my $item = $array[0];
	if ($url eq 'LIFT' || $url eq '') {
		print "\t$item : NO URL ('lift')\n";
		$lifted++;
		$failed++;
		next;
	}
	print "\t$item from $url : ";
	if ($url !~ m/\.gz$/i) {
		`wget -q -O /tmp/$item.txt '$url'`;
	}
	else {
		`wget -q -O /tmp/$item.txt.gz '$url' && gunzip /tmp/$item.txt.gz --force`;
	}
	if (-z "/tmp/$item.txt" || -e "/tmp/$item.txt.gz") {
		print " FAILED\n";
		$failed++;
	}
	else {
		print " OK\n";
	}
	if ($item =~ m/Manual/) {
		$manual = $item;
	}
	elsif ($item =~ m/Auto/) {
		$auto = $item;
	}
	elsif ($item =~ m/Classes/) {
		$classes = $item;
	}
}
if ($failed > 0) {
	if ($opts{'L'}) {
		# perform liftover!
		if (!$opts{'d'}  || !$opts{'D'}) {
			print "\t##############################################\n";
			print "\t# INSUFFICIENT INFO FOR LIFT OVER COMMAND    #\n";
			print "\t#  => source (d) or target (D) build missing #\n";
			print "\t##############################################\n";
			print "\t sourcedb: $sourcedb\n";
			print "\t targetdb: $targetdb\n";
			print "\n";
			print "\t################\n";
			print "\t# NOT UPDATING #\n";
			print "\t################\n";
			exit;
		}
		# ok, start lift over command.
		print "\t##############################\n";
		print "\t# STARTING LIFT OVER PROCESS #\n";
		print "\t##############################\n";
		system("cd LiftOver/ && ./LiftOver.pl -U $uid -d '$sourcedb' -D '$targetdb' -T encode");
		# summarise
		print "Final Step: Create Summary Table\n";
		&Summarise;
		print "\n";
		print "###################\n";
		print "# UPDATE FINISHED #\n";
		print "###################\n";
		exit;

	}
	elsif ($lifted < $failed) {
		print "\n";
		print "\t####################################################\n";
		print "\t# SOME FILES FAILED TO DOWNLOAD, CHECK YOUR URLs ! #\n";
		print "\t####################################################\n";
		print "\n";
		print "\t################\n";
		print "\t# NOT UPDATING #\n";
		print "\t################\n";
		exit;

	}
	else {
		print "\n";
		print "\tSome annotation sources are not provided. This track was lifted from a previous build.\n";
	
		print "\t################\n";
		print "\t# NOT UPDATING #\n";
		print "\t################\n";
		exit;
	}
}
print "\t=> All files successfully downloaded\n";

# FILL THE TABLE

$query = "TRUNCATE TABLE encode ";
$dbh->do($query);

open IN, "/tmp/$manual.txt";
my $nrgenes = 0;
print "3/ Inserting Manual Annotation table information\n";
while (<IN>) {
	
	my $line = $_;
	chomp($line);
	my @pieces = split(/\t/,$line);
    # non canonical chromosome
	if ($pieces[2] =~ m/_/ ) {
		next;
	}
	else {
		my $refname = $pieces[12];	#ok : ENSG...
		my $chrom = $pieces[2];		#ok
		my $start = $pieces[4];		#ok
		my $end = $pieces[5];		#ok
		my $strand = $pieces[3];	#ok
		my $enstname = $pieces[1];	#ok (was nmname) ENST...
		$chrom =~ s/chr//;		 
		$chrom = $chromhash{ $chrom };	
		my $exoncount = $pieces[8];	#ok
		my $exonstarts = $pieces[9];	#ok
		my $exonends = $pieces[10];	#ok
		my $cdsstartstat = $pieces[13];	#ok
		my $cdsendstat = $pieces[14];	#ok
		my $coding;
		if ($cdsstartstat eq 'none' && $cdsendstat eq 'none') {
			$coding = 0;
		}
		else {
			$coding = 1;
		}
		my $query = "INSERT INTO encode (symbol, chr, start, end, strand, etID, nrexons, exonstarts, exonends, coding) values ('$refname', '$chrom', '$start', '$end', '$strand', '$enstname', '$exoncount', '$exonstarts', '$exonends', '$coding') ";
		$dbh->do($query);
		$nrgenes++;
	}
}
close IN;
##unlink("/tmp/$manual.txt");
print "\t=> Done\n";
open IN, "/tmp/$auto.txt";
print "4/ Inserting Automatic Annotation table information\n";
while (<IN>) {
	
	my $line = $_;
	chomp($line);
	my @pieces = split(/\t/,$line);
	if ($pieces[2] =~ m/_/ ) {
		next;
	}
	else {
		my $refname = $pieces[12];	#ok
		my $chrom = $pieces[2];		#ok
		my $start = $pieces[4];		#ok
		my $end = $pieces[5];		#ok
		my $strand = $pieces[3];	#ok
		my $enstname = $pieces[1];	#ok (was nmname)
		$chrom =~ s/chr//;		
		$chrom = $chromhash{ $chrom };	
		my $exoncount = $pieces[8];	#ok
		my $exonstarts = $pieces[9];	#ok
		my $exonends = $pieces[10];	#ok
		my $cdsstartstat = $pieces[13];	#ok
		my $cdsendstat = $pieces[14];	#ok
		my $coding;
		if ($cdsstartstat eq 'none' && $cdsendstat eq 'none') {
			$coding = 0;
		}
		else {
			$coding = 1;
		}
		my $query = "INSERT INTO encode (symbol, chr, start, end, strand, etID, nrexons, exonstarts, exonends, coding) values ('$refname', '$chrom', '$start', '$end', '$strand', '$enstname', '$exoncount', '$exonstarts', '$exonends', '$coding') ";
		$dbh->do($query);
		$nrgenes++;
	}
}
close IN;
#unlink("/tmp/$auto.txt");
print "\t=> Done\n";

open IN, "/tmp/$classes.txt";
print "5/ Inserting classification\n";
while (<IN>) {
	
	my $line = $_;
	chomp($line);
	my @pieces = split(/\t/,$line);
	my $egid = $pieces[0];	#ok
	my $enstname = $pieces[1];	#ok (was nmname)
	my $type = $pieces[2];	#ok
	my $level = $pieces[3];
	my $class = $pieces[4];
	if ($class =~ m/Havana/) {
		$ottG = $pieces[5];
	}
	else {
		$ottG = '';
	}
	my $query = "UPDATE encode SET egID = '$egid', type = '$type', level = '$level', class = '$class', ottgid = '$ottG' WHERE etID = '$enstname' ";
	$dbh->do($query);
	
}
close IN;


#unlink("/tmp/$classes.txt");
print "\t=> Done\n";
print "6/ Creating Summary Table\n";
&Summarise;
print "\t###################\n";
print "\t# UPDATE FINISHED #\n";
print "\t###################\n";

###############
# SUBROUTINES #
###############

sub Summarise {
# CREATE SUMMARY TABLE
my $trunc = "TRUNCATE TABLE encodesum";
$dbh->do($trunc);

my $query = "SELECT chr, start, end, strand, symbol, exonstarts, exonends, coding, level FROM encode ORDER BY coding, symbol, chr, start";
$sth = $dbh->prepare($query);
$sth->execute();
$firststart = 0;
$laststop = 0;
$prevchr = 1;
$prevsymb = '';
$entries = 0;
$prevstrand = '';
$prevcoding;
$prevlevel;
my %exonhash; 
while (my @row = $sth->fetchrow_array()) {
	my $chr = $row[0];
	my $chrtxt = $chromhash{ $chr };
	my $start = $row[1];
	my $stop = $row[2];
	my $strand = $row[3];
	my $symbol = $row[4];
	my $exonstart = $row[5];
	my $exonend = $row[6];
	my @exonstarts = split(/,/,$exonstart);
	my @exonends = split(/,/,$exonend);
	my $coding = $row[7];
	my $level = $row[8];
	#print "input line: $symbol : $chr:$start-$stop\n";
	if ($symbol ne $prevsymb) {
		# need to write out previous symbol !
		#print "$prevsymb summary: $prevchr:$firststart-$laststop ($entries entries) \n";
		if ($prevsymb ne '') {
			my $exstartstring = '';
			my $exendstring = '';
			foreach my $key (sort keys %exonhash) {
				$exstartstring .= "$key,";
				$exendstring .= $exonhash{$key} . ",";
			}
			$exstartstring = substr($exstartstring,0,-1);
			$exendstring = substr($exendstring,0,-1);
			$query = "INSERT INTO encodesum (chr, start, stop, strand, symbol,exonstarts,exonends,coding,level,entries) VALUES ('$prevchr', '$firststart', '$laststop', '$prevstrand', '$prevsymb', '$exstartstring','$exendstring','$prevcoding','$prevlevel','$entries')";
			$dbh->do($query);
			%exonhash = ();
		}
		$prevstart = $start;
		$firststart = $start;
		$prevstop = $stop;
		$laststop = $stop;
		$prevchr = $chr;
		$prevsymb = $symbol;
		$prevstrand = $strand;
		$prevcoding = $coding;
		$prevlevel = $level;
		my $idx = 0;
		foreach(@exonstarts) {
			if ($exonhash{$_} < $exonends[$idx]) {
				$exonhash{$_} = $exonends[$idx];
			}
			$idx++;
		}
		$entries = 1;
		next;
	}
	if ($start > $laststop && $laststop != 0) {
		#print "$prevsymb summary: $prevchr:$firststart-$laststop ($entries entries) \n";
		if ($prevsymb ne '') {
			my $exstartstring = '';
			my $exendstring = '';
			foreach my $key (sort keys %exonhash) {
				$exstartstring .= "$key,";
				$exendstring .= $exonhash{$key} . ",";
			}
			$exstartstring = substr($exstartstring,0,-1);
			$exendstring = substr($exendstring,0,-1);
			$query = "INSERT INTO encodesum (chr, start, stop, strand, symbol,exonstarts, exonends,coding,level,entries) VALUES ('$prevchr', '$firststart', '$laststop', '$prevstrand', '$prevsymb', '$exstartstring','$exendstring','$prevcoding','$prevlevel','$entries')";
			$dbh->do($query);
		}

		$firststart = $start;
		$laststop = $stop;
		$prevcoding = $coding;
		if ($level < $prevlevel) {
			$prevlevel = $level;
		}
		my $idx = 0;
		foreach(@exonstarts) {
			if ($exonhash{$_} < $exonends[$idx]) {
				$exonhash{$_} = $exonends[$idx];
			}
			$idx++;
		}

		$entries = 1;
		next;
	}
	if ($start >= $laststop && $laststop == 0) {
		#print "first on the chromosome $chrtxt\n";
		$firststart = $start;
		$laststop = $stop;
		$prevcoding = $coding;
		$entries++;
		my $idx = 0;
		if ($level < $prevlevel) {
			$prevlevel = $level;
		}

		foreach(@exonstarts) {
			if ($exonhash{$_} < $exonends[$idx]) {
				$exonhash{$_} = $exonends[$idx];
			}
			$idx++;
		}

		next;
	}
	if ($stop > $laststop) {
		$laststop = $stop;
		$entries++;
		$prevcoding = $coding;
		my $idx = 0;
		if ($level < $prevlevel) {
			$prevlevel = $level;
		}

		foreach(@exonstarts) {
			if ($exonhash{$_} < $exonends[$idx]) {
				$exonhash{$_} = $exonends[$idx];
			}
			$idx++;
		}

		next;
	}
	$entries++;
}
#print "$prevsymb summary: $prevchr:$firststart-$laststop ($entries entries) \n";
if ($prevsymb ne '') {
	my $exstartstring = '';
	my $exendstring = '';
	foreach my $key (sort keys %exonhash) {
		$exstartstring .= "$key,";
		$exendstring .= $exonhash{$key} . ",";
	}
	$exstartstring = substr($exstartstring,0,-1);
	$exendstring = substr($exendstring,0,-1);
	$query = "INSERT INTO encodesum (chr, start, stop, strand, symbol,exonstarts,exonends,coding,level, entries) VALUES ('$prevchr', '$firststart', '$laststop', '$prevstrand', '$prevsymb','$exstartstring','$exendstring','$prevcoding','$prevlevel','$entries')";
	$dbh->do($query);
}
print "\t=> Done\n\n";
}




