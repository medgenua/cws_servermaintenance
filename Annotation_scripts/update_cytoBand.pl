#!/usr/bin/perl
use Getopt::Std;
use DBI;

# read credentials
use Cwd 'abs_path';
$credpath = abs_path("../LoadCredentials.pl");
require($credpath);


$|++;

####################
# DEFINE VARIABLES #
####################
my %chromhash = ();
my @chroms = (1..22);
foreach (@chroms)  {
	$chromhash{ $_ } = $_;
}
$chromhash{ "23" } = "X";
$chromhash{ "24" } = "Y"; 
$chromhash{ "X" } = "23";
$chromhash{ "Y" } = "24";
$chromhash{ "M" } = "25";
$chromhash{ "25" } = "M";

##########################
# COMMAND LINE ARGUMENTS #
##########################
# L = Looking at a new database, LiftOver old values if needed
# D = Use specific database (mandatory if L is specified), otherwise use default.
# d = Database to use as source (mandatory if L is specified.
# u = User ID, mandatory

getopts('LD:d:u:', \%opts);  # option are in %opts

# print name of script and what will happen
print "######################################\n";
print "# UPDATING CytoBand ANNOTATION TABLE #\n";
print "######################################\n";
print "\n";

################################### 
# CONNECT TO CNVanalysis DATABASE #
###################################
my $db = "";
print "1/ Connecting to the database\n";

if ($opts{'D'}){
	$db = "CNVanalysis".$opts{'D'};
	$targetdb = $opts{'D'};
	print "\t=> Using user specified database $db\n";
}
else {
	# connect to GenomicBuilds DB to get the current Genomic Build Database. 
	my $dbhgb = DBI->connect("dbi:mysql:GenomicBuilds:$host",$userid,$userpass);
	my $gbsth =$dbhgb->prepare("SELECT name, StringName FROM CurrentBuild");
	$gbsth->execute();
	my @row = $gbsth->fetchrow_array();
	$gbsth->finish();
	$db = "CNVanalysis".$row[0];
	$newbuild = $row[1];
	print "\t=> Using current build database (build ".$row[1].")\n";	
}
$connectionInfocnv="dbi:mysql:$db:$host"; 
$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
if ($opts{'d'}) {
	$sourcedb = $opts{'d'};
}
if ($opts{'u'}) {
	$uid = $opts{'u'};
}
else {
	$uid = 1;
}

########################
# FETCH DATA TABLE URL #
########################
my $query = "SELECT url FROM Administration WHERE item = 'cytoBand'";
my $sth = $dbh->prepare($query);
$sth->execute();
my @row = $sth->fetchrow_array();
my $url = $row[0];
$sth->finish();

####################
# START PROCESSING #
####################
if (($url eq 'LIFT' || $url eq '') && $opts{'L'}) {
	# no url, and liftOver command specified
	print "2/ LiftOver Build $sourcedb to $targetdb\n"; 
	print "\t##############################\n";
	print "\t# STARTING LIFT OVER PROCESS #\n";
	print "\t##############################\n";
	system("cd ../LiftOver/ && ./LiftOver.pl -U $uid -d '$sourcedb' -D '$targetdb' -T 'cytoBand'");
	print "\n";
	print "###################\n";
	print "# UPDATE FINISHED #\n";
	print "###################\n";
	exit;

}
elsif ($url eq 'LIFT' || $url eq '') {
	# no url, and liftOver command NOT specified, skip update.
	print "2/ No URL available for this annotation track\n";
	print "\t=> Not updating \n";
	print "\n";
	print "###################\n";
	print "# UPDATE FINISHED #\n";
	print "###################\n";
}
else {
	# url available: proceed
	print "2/ Downloading data table\n";
	print "\t=> From $url: ";
	if (-e '/tmp/cytoBand.txt') {
		unlink('/tmp/cytoBand.txt');
	}
	`wget -v -O /tmp/cytoBand.txt.gz $url && gunzip /tmp/cytoBand.txt.gz --force`;
	if (! -f "/tmp/cytoBand.txt" || -z "/tmp/cytoBand.txt" || -e "/tmp/cytoBand.txt.gz") {
		print "\t=> DOWNLOAD FAILED\n";
		print "\t=> Not updating\n";
		print "###################\n";
		print "# UPDATE FINISHED #\n";
		print "###################\n";
		die("DOWNLOAD FAILED");

	}
	print "OK.\n";
	print "\t=> Done\n";
	print "3/ SCANNING & INSERTING DATA: \n";
	# note: study table is never truncated, study id's are maintained to keep usersettings correct.
	my $trunc = "TRUNCATE TABLE cytoBand";
    print("Truncating cytoBand table..");
	$dbh->do($trunc);
	open IN, "/tmp/cytoBand.txt";
    print("Parsing new data");
	while (my $line = <IN>) {
		chomp($line);
		if ($line eq '') {
			next;
		}
		my @parts = split(/\t/,$line);
		my $chrtxt = $parts[0];
		$chrtxt =~ s/chr//;
		my $start = $parts[1];
		my $stop = $parts[2];
		my $name = $parts[3];
		my $stain = $parts[4];
		my $chr = $chromhash{$chrtxt};
		my $query = "INSERT INTO cytoBand (chr, start, stop, name, gieStain) VALUES ('$chr', '$start', '$stop', '$name', '$stain')";
		$dbh->do($query);
	}
	close IN;
	unlink('/tmp/cytoBand.txt');
	print "\t=>Done\n";
	print "###################\n";
	print "# UPDATE FINISHED #\n";
	print "###################\n";

}

