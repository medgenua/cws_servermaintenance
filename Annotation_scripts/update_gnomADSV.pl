#!/usr/bin/perl

$|++;

use Getopt::Std;
use DBI;
# read credentials
use Cwd 'abs_path';
$credpath = abs_path("../LoadCredentials.pl");
require($credpath);

# DEFINE VARIABLES
my %chromhash = ();
my @chroms = (1..22);
foreach (@chroms)  {
	$chromhash{ $_ } = $_;
}
$chromhash{ "23" } = "X";
$chromhash{ "24" } = "Y";
$chromhash{ "X" } = "23";
$chromhash{ "Y" } = "24";
$chromhash{ "M" } = "25";
$chromhash{ "MT" } = "25";
$chromhash{ "25" } = "M";
##########################
# COMMAND LINE ARGUMENTS #
##########################
# L = Looking at a new database, LiftOver old values if needed
# D = Use specific database (mandatory if L is specified), otherwise use default.
# d = Database to use as source (mandatory if L is specified.
# u = User ID, mandatory

my %opts;
getopts('LD:d:u:', \%opts);  # option are in %opts

# print name of script and what will happen
print "#########################################\n";
print "# UPDATING TORONTO DGV ANNOTATION TABLE #\n";
print "#########################################\n";
print "\n";

###################################
# CONNECT TO CNVanalysis DATABASE #
###################################
my $db = "";

print "1/ Connecting to the database\n";

my ($targetdb, $newbuild, $connectionInfocnv, $dbh, $sourcedb, $uid);
if ($opts{'D'}){
	$db = "CNVanalysis".$opts{'D'};
	$targetdb = $opts{'D'};
	print "\t=> Using user specified database $db\n";
}
else {
	# connect to GenomicBuilds DB to get the current Genomic Build Database.
	my $dbhgb = DBI->connect("dbi:mysql:GenomicBuilds:$host",$userid,$userpass);
	my $gbsth =$dbhgb->prepare("SELECT name, StringName FROM CurrentBuild");
	$gbsth->execute();
	my @row = $gbsth->fetchrow_array();
	$db = "CNVanalysis".$row[0];
	$newbuild = $row[1];
	print "\t=> Using current build database (build ".$row[1].")\n";
}
$connectionInfocnv="dbi:mysql:$db:$host;mysql_local_infile=1";
$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
if ($opts{'d'}) {
	$sourcedb = $opts{'d'};
}
if ($opts{'u'}) {
	$uid = $opts{'u'};
}
else {
	$uid = 1;
}

########################
# FETCH DATA TABLE URL #
########################
my $query = "SELECT url FROM Administration WHERE item = 'gnomadsv'";
my $sth = $dbh->prepare($query);
$sth->execute();
my @row = $sth->fetchrow_array();
my $url = $row[0];
$sth->finish();

####################
# START PROCESSING #
####################
if (($url eq 'LIFT' || $url eq '') && $opts{'L'}) {
	# no url, and liftOver command specified
	print "2/ LiftOver Build $sourcedb to $targetdb\n";
	print "\t##############################\n";
	print "\t# STARTING LIFT OVER PROCESS #\n";
	print "\t##############################\n";
	system("cd LiftOver/ && ./LiftOver.pl -U $uid -d '$sourcedb' -D '$targetdb' -T 'DGV_full'");
	print "\n";
	print "###################\n";
	print "# UPDATE FINISHED #\n";
	print "###################\n";
	exit;

}
elsif ($url eq 'LIFT' || $url eq '') {
	# no url, and liftOver command NOT specified, skip update.
	print "2/ No URL available for this annotation track\n";
	print "\t=> Not updating \n";
	print "\n";
	print "###################\n";
	print "# UPDATE FINISHED #\n";
	print "###################\n";
}
else {
	# url available: proceed
	print "2/ Downloading gnomAD SV table\n";
	print "\t=> From $url: ";
	if (-e '/tmp/gnomadsv.vcf.gz') {
		unlink('/tmp/gnomadsv.vcf.gz');
    }
    if (-e '/tmp/gnomadsv.vcf') {
		unlink('/tmp/gnomadsv.vcf');
    }

	`wget -q -O /tmp/gnomadsv.vcf.gz "$url" `;
	
	my $recheck = 0;
	if (-z '/tmp/gnomadsv.vcf.gz') {
		print " File not found, trying to recover\n";
		`wget -q -O /tmp/gnomadsv.vcf.gz "$url" `;
		$recheck = 1;
	}
	if ($recheck == 1) {
		if (-z '/tmp/gnomadsv.vcf.gz') {
			die("\t=> gnomAD SV Data file not found !! check your links from the Administration pages!\n\n\t=>UPDATE FAILED !!!!\n");
		}
	}
	
    # unpack
    system("cd /tmp/ ; gunzip gnomadsv.vcf.gz") == 0 or die("Failed to unzip gnomad vcf file.\n\n\n=> UPDATE FAILED\n");
	print " OK\n";
    my $file = '/tmp/gnomadsv.vcf';
	################
	# PARSE & LOAD #
	################
    print "3/ parse input data.\n";
    open IN, $file or die("could not open file : $file\n"); 
    open CNV, ">/tmp/load.gnomadsv.cnv.txt";
    open CMPX, ">/tmp/load.gnomadsv.cmpx.txt";
    $nrlines = 0;
    # it's a vcf. complex events might be on multiple lines.
    my %seen = ();
    while (my $line = <IN>) {
        chomp($line);
        if (substr($line,0,1) == '#') {
            next;
        }
        $nrlines += 1;
        # columns
        my @c = split("\t",$line);
        $c[2] =~ s/gnomAD-SV_v2.1_//;
        my %info = ();
        # main info is in column 8 as k=v;k=v; ..
        my @p = split(";",$c[7]);
        foreach (@p) {
            my @i = split("=",$_);
            $info{$i[0]} = $i[1];
        }
        #if (defined($seen{$info{'ID'}})) {
        #    next;
        #}
        #$seen{$info{'ID'}} = 1;
        # clean up chr
        $c[0] =~ s/chr//;
        # split by type : SINGLE CHROM / LINEAR EVENTS 
        if ($info{'SVTYPE'} =~ m/DUP|INS|DEL|INV/) {
            # id , chr, start, stop, type,
            my $outline = $c[2] . "\t". $chromhash{$c[0]} . "\t" ;
              $outline .= $c[1] . "\t" . $info{"END"} . "\t";
              $outline .= $info{'SVTYPE'} . "\t";
            # qual , filter
              $outline .= $c[5] . "\t";
              $outline .= $c[6] . "\t";
            # frequencies
              $outline .= $info{'N_HOMREF'} . "\t";
              $outline .= $info{'N_HET'} . "\t";
              $outline .= $info{'N_HOMALT'} . "\t";
              $outline .= $info{'AF'} . "\t";
            # evidence 
              $outline .= $info{'EVIDENCE'} . "\t";
            # and max pop specific frequency.
              $outline .= $info{'POPMAX_AF'} ."\n";
            print CNV $outline;
        } else {
            #print(" SV type : $info{'SVTYPE'}\n");
        }

        
        
        

    }
    close IN;
    close CNV;

    # LOAD
    $dbh->do("TRUNCATE TABLE `gnomAD_SV_CNV`");
    $dbh->do("LOAD DATA LOCAL INFILE '/tmp/load.gnomadsv.cnv.txt' INTO TABLE `gnomAD_SV_CNV` ");
	# clean
    #system("rm -f /tmp/toronto.txt /tmp/load.toronto.txt");
	print "\t=> $nrlines items were stored\n";
	print "\n";
	print "###################\n";
	print "# UPDATE FINISHED #\n";
	print "###################\n";
	#unlink($file);
}
