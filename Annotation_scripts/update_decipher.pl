#!/usr/bin/perl
use Getopt::Std;
use DBI;
use Net::FTP;

# read credentials
use Cwd 'abs_path';
$credpath = abs_path("../LoadCredentials.pl");
require($credpath);

$| = 1;

my %chromhash = ();
my @chroms = (1..22);
foreach (@chroms)  {
	$chromhash{ $_ } = $_;
}
$chromhash{ "X" } = 23;
$chromhash{ "Y" } = 24; 

##########################
# COMMAND LINE ARGUMENTS #
##########################
# L = Looking at a new database, LiftOver old values if needed
# D = Use specific database (mandatory as target db if L is specified), otherwise use default.
# d = Database to use as source (mandatory if L is specified.
# U = Just update number of disrupted genes for all aberrations in the database.
# u = User ID, mandatory
# S = Just recreate summary table & update aberrations (daily, for pubmed citations, omim data, etc)
my %opts;
getopts('SLUD:u:d:', \%opts);  # option are in %opts

# print name of script and what will happen
print "###########################\n";
print "# UPDATING DECIPHER TABLE #\n";
print "###########################\n";
print "\n";

# connect to database
my $db = "";
print "1/ Connecting to the database\n";
my ($ucscbuild, $targetdb, $newbuild, $sourcedb, $uid);
if ($opts{'D'}){
	$db = "CNVanalysis".$opts{'D'};
	$targetdb = $opts{'D'};
	$ucscbuild = $opts{'D'};
	$ucscbuild =~ s/^-//;
	print "\t=> Using user specified database $db\n";
}
else {
	# connect to GenomicBuilds DB to get the current Genomic Build Database. 
	my $dbhgb = DBI->connect("dbi:mysql:GenomicBuilds:$host",$userid,$userpass);
	my $gbsth =$dbhgb->prepare("SELECT name, StringName FROM CurrentBuild");
	$gbsth->execute();
	my @row = $gbsth->fetchrow_array();
	$gbsth->finish();
	$db = "CNVanalysis".$row[0];
	$newbuild = $row[1];
	$ucscbuild = $row[0];
	$ucscbuild =~ s/^-//;
	print "\t=> Using current build database (build ".$row[1].")\n";	
}
my $connectionInfocnv="dbi:mysql:$db:$host";
my $dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
if ($opts{'d'}) {
	$sourcedb = $opts{'d'};
}
if ($opts{'u'}) {
	$uid = $opts{'u'};
}
else {
	$uid = 1;
}
# download the datafiles
print "2/ Downloading the datafiles\n";
my $urlsth = $dbh->prepare("SELECT item, url FROM Administration WHERE UpdateScript = 'update_decipher.pl'");
my $urlrows = $urlsth->execute();
my $failed = 0;
if ($urlrows == 0) {
	print "\t=> No URLs specified\n";
	&lift;
}
my @array = $urlsth->fetchrow_array();
my $url = $array[1];
my $item = $array[0];
if ($url eq 'LIFT' || $url eq '') {
	print " NO URL ('lift')\n";
	$failed = 1;
	&lift;
}
print "\t=> $item from $url : ";
if ($decuser eq '' || $decpass eq '' || $decgpgpass eq '') {
	die('Can not update, decipher DDA user and password are not specified in ServerMaintenance/.credentials.');
}
# clean target tmp directory
my $dectmpdir = "/tmp/update_decipher/";
if (-d $dectmpdir) {
	system("rm -Rf $dectmpdir");
}
system("mkdir $dectmpdir");


# fetch file from decipher sftp server
my $command = "sshpass -p '$decpass' sftp $decuser\@$url/decipher-cnvs-grch37-*.txt.gpg $dectmpdir > /dev/null 2>&1";
system($command);
my $out = `echo $?`;
chomp($out);
if ($out ne 0) {
    print "FAILED\n";
    die("Fetching DECIPHER file from SFTP server failed");
}
print "OK\n";

my $decipherfile = `ls $dectmpdir`;
chomp($decipherfile);
# decrypt.
print "3/ Decrypting the input file \n";
$command = "cd $dectmpdir && echo $decgpgpass | gpg -q --no-tty --passphrase-fd 0 $decipherfile >/dev/null 2>&1";
system($command) and die("Couldn't decrypt the source file");
# update input filename
$decipherfile =~ s/\.gpg//;

if (-d 'DecipherFiles') {
    # check if this is a new release
    my $latest = `cat DecipherFiles/latest.release.txt`;
    chomp($latest);
    print "The most recent file available is : $decipherfile\n";
    print "The latest update was with release : $latest\n";
    if ($latest eq $decipherfile) {
        print "This is the same version, so updating is not needed. Program will exit.\n";
        exit();
    }
}
else {
    print "directory non existent; making\n";
    system("mkdir DecipherFiles");
    system("echo '$decipherfile' > DecipherFiles/latest.release.txt");
}

print "\t=> Input file $decipherfile is ready for processing\n";



#####################
## CLEAN OUT TABLE ##
#####################
# patients
$dbh->do("TRUNCATE TABLE decipher_patients");
# syndromes
#  => not supported, not present on ftp. DO NOT TRUNCATE
###################
## LOAD NEW DATA ##
###################
print "4/ Scanning input file\n";
my %pheno;  # will contain case_id => phenotype array (hash of arrays)
my %regions; # will contain case_id => region array (hash of arrays);

open IN, "/tmp/update_decipher/$decipherfile";
my $header = <IN>;
$header = <IN>;

while (<IN>) {
	my $line = $_;
	chomp($line);
	my @p = split(/\t/,$line);
	my $case = $p[0];
	my $start = $p[2];
	my $stop = $p[3];
	my $chr = $p[1];
	$chr = $chromhash{$chr};
	my $meanratio = $p[4];
	my $class = $p[7];
	my $phenotypes = $p[10];
	my $region = "$chr|$start|$stop|$meanratio|$class";
	# add phenotype to hash
	foreach my $phenotype (split(/\|/, $phenotypes)) {
		if ($pheno{$case}) {
			if(grep $_ eq $phenotype, @{$pheno{$case}}) {
				# already in array, skip
			}
			else {
				push(@{$pheno{$case}}, $phenotype);
			}
		}
		else {
        		push(@{$pheno{$case}}, $phenotype);
		}
	}
	# add region to hash
	if ($regions{$case}) {
		if(grep $_ eq $region, @{$regions{$case}}) {
			print "DUPLICATE region:\n";
			print "$line\n";
			print "------------\n";
			# already in array, skip
		}
		else {
			push(@{$regions{$case}}, $region);
		}
	}
	else {
	        push(@{$regions{$case}}, $region);
	}

} 
close IN;
### dont use pheno hash here, else you skip all the samples that dont have a phenotype
my $nrcases = keys(%regions);
print "\t=> Done ($nrcases cases found)\n";



###########################
## NOW STORE TO DATABASE ##
###########################
print "5/ Sending data to database\n";
my $sth = $dbh->prepare("INSERT INTO decipher_patients (chr, start, stop, caseid, inheritance, type, phenotype) VALUES (?,?,?,?,?,?,?)");
foreach my $case (keys %regions) {
	#print "Phenotypes For Case $case : \n";
	my $phenostring = '';
	foreach (@{$pheno{$case}}) {
		if ($_ ne ''){
	 		$phenostring .= $_."; ";
		}
	}
	if ($phenostring ne '') {
		$phenostring = substr($phenostring,0,-2);
	}
	
	#print "Regions For Case $case : \n";
	foreach (@{$regions{$case}}) {
		my @p = split(/\|/,$_);
		my $chr = $p[0];
		my $start = $p[1];
		my $stop = $p[2];
		my $type;
		if ($p[3] < 0) {
			$type = 'del';
		}
		else {
			$type = 'dup';
		}
		my $inh = $p[4];
		$sth->execute($chr,$start,$stop,$case,$inh,$type,$phenostring);
	}

}
$sth->finish();
print "\t=>Done\n";

print "6/ Cleaning up\n";
system("rm -Rf /tmp/update_decipher");
print "\t=> Done\n";
print "\n";
print " #####################\n";
print " ## UPDATE FINISHED ##\n";
print " #####################\n";


# store latest file version for future updates
system("echo '$decipherfile' > DecipherFiles/latest.release.txt");

#create summary table
## looks buggy, purpose of script?
#system("perl create_decipher_sumtable.pl");

#exit(1);



sub lift {
	print "lifting subroutine to come!";
	exit;
}
