#!/usr/bin/perl

use Getopt::Std;
use DBI;
use Sys::CPU;
use threads;
use Thread::Queue;
use threads::shared;
use Sys::CpuLoad;

# read credentials
use Cwd 'abs_path';
$credpath = abs_path("../LoadCredentials.pl");
require($credpath);

$|++;
# DEFINE VARIABLES
my %chromhash = ();
my @chroms = (1..22);
foreach (@chroms)  {
	$chromhash{ $_ } = $_;
}
$chromhash{ "23" } = "X";
$chromhash{ "24" } = "Y";
$chromhash{ "25" } = "M";
$chromhash{ "X" } = "23";
$chromhash{ "XY" } = "23";
$chromhash{ "Y" } = "24";
$chromhash{ "M" } = "25";
$chromhash{ "MT" } = "25";
##########################
# COMMAND LINE ARGUMENTS #
##########################
# d = Source Database, mandatory.
# D = Target Database, mandatory.
# C = LiftOver Chain file, can be fetched if present in database
# T = Table to Lift, mandatory
# G = Max Gap (default 10000) in basepairs to merge fragments
# F = Min Fraction (default 0.9) to retain fragment if partially deleted 
# U = platform userid, needed for logfiles
# p = Provided only for lifting Probelocations table: files with probelocations in new build, seperated by '@@'
getopts('D:d:C:T:G:F:U:p:', \%opts);  # option are in %opts

########################### 
# CHECK MANDATORY OPTIONS #
###########################
if (!$opts{'D'} || $opts{'D'} eq '') {
	die('Target Database is mandatory');
}
if (!$opts{'d'} || $opts{'d'} eq '') {
	die('Source Database is mandatory');
}
if (!$opts{'T'} || $opts{'T'} eq '') {
	die('Tablename is mandatory');
}
$sourcedb = "CNVanalysis" . $opts{'d'};
$targetdb = "CNVanalysis" . $opts{'D'};

############################## 
# CONNECT TO SOURCE DATABASE #
##############################
print "1/ Connecting to the Source database ($sourcedb)\n";
my %attr = (PrintError => 0,RaiseError => 0,mysql_auto_reconnect => 1,mysql_local_infile => 1);
$connectionInfocnv="dbi:mysql:$sourcedb:$host"; 
$dbh = DBI->connect($connectionInfocnv,$userid,$userpass,\%attr) or die('Could not connect to database') ;
print "\t=> Done\n";

########################
# CHECK FOR CHAIN FILE #
########################
if (!$opts{'C'} || $opts{'C'} eq '' || not -e $opts{'C'} ) {
	my $query = "SELECT url, filename FROM `GenomicBuilds`.`LiftFiles` WHERE frombuild = '".$opts{'d'}."' AND tobuild = '".$opts{'D'}."'";
	my $sth = $dbh->prepare($query);
	my $rows = $sth->execute();
	if ($rows == 0) {	
		die('Chain File is is not specified and not present on the system');
	}
	my @row = $sth->fetchrow_array();
	my $url = $row[0];
	my $filename = $row[1];
	if (-s "$filename" && $filename ne '') {
		$chainfile = $filename;
		print "  => Found ChainFile in the database ($chainfile)\n";
	}
	elsif ($url ne '' && $filename ne '') {
		if (-e "$filename" ) {
			unlink("$filename");
		}
		print "\t=> Need to fetch the chainfile: ";
		system("wget -q -O '$filename.gz' '$url' && gunzip '$filename.gz' --force");
		if (-z "$filename" || -e "$filename.gz") {
			die('Chain File Could not be fetched from the internet');
		}
		print "OK\n";
		$chainfile = $filename;
	}
	else {
		# risky, try to compose url and filename ourselves.
		print "\t=> Need to search for the chainfile online (from ad hoc url): ";
		my $tempsource = $opts{'d'};
		$tempsource =~ s/^-//;
		my $temptarget = $opts{'D'};
		$temptarget =~ s/^-//;
		$temptarget =~ s/^hg/Hg/;
		my $url ="http://hgdownload.cse.ucsc.edu/goldenPath/$tempsource/liftOver/$tempsource"."To$temptarget".".over.chain.gz";
		my $tempfile = $tempsource."To$temptarget".".over.chain";
		if (-s "$tempfile") {
			$chainfile = $tempfile;
			print "OK, file was already present\n";
		}
		else {
			system("wget -q -O '$tempfile.gz' '$url' && gunzip '$tempfile.gz' --force");
			if (-z "$tempfile" || -e "$tempfile.gz") {
				die('Chain File Could not be fetched from the internet');
			}
			print "OK\n";
			$chainfile = $tempfile;
		}
	}	
}
else {
	$chainfile = $opts{'C'};
}
# set values
if ($opts{'G'}) {
	$Gap = $opts{'G'};
}
else {
	$Gap = 10000;
}
if ($opts{'F'}) {
	$retain = $opts{'F'};
}
else {
	$retain = 0.9;
}
if ($opts{'U'}) {
	$uid = $opts{'U'};
}
else {
	$uid = 1;
}
$table = $opts{'T'};

if ($table =~ m/deletedcnvs/) {
	print "\n\n";
	print "###########################################################\n";
	print "# The tables containing deleted CNVs can not be Lifted !  #\n";
	print "# Coordinates of deleted entries are always kept in       #\n";
	print "# their final genomic build and not lifted afterwards.    #\n";
	print "###########################################################\n";
	exit;
}
#print "Lifting table '$table' From '$sourcedb' To '$targetdb'\n";


#############################################
# GET COLUMN HEADERS, EXTRACT POSITION COLS #
#############################################
print "2/ Determining the position and identifier columns for table '$table'\n";
my $sth = $dbh->prepare("SELECT * FROM $table WHERE 1=0");
$sth->execute;
my @fields = @{$sth->{NAME}};
$sth->finish;
my $chrcol = '';
my $startcol = '';
my $stopcol = '';
my $idcol = '';
my $liftable = 0;
foreach (@fields) {
	$colname = $_;
	if ($colname =~ m/^chr/i) {
		$chrcol = $colname;
	}
	if ($colname =~ m/^(start|position)$/i) {
		$startcol = $colname;
	}
	if ($colname =~ m/^(stop|end|position)$/i) {
		$stopcol = $colname;
	}
	if($colname =~ m/^id$/i || ($colname eq 'rid' && $table eq 'Filters_x_Regions')) {
		$idcol = $colname;
	}
	if ($colname eq 'LiftedFrom') {
		$liftable = 1;
	}
}
if ($liftable == 0) {
	die("$table is considered Genome Build Independent (no LiftedFrom Column");
}
if ($chrcol eq '' || $startcol eq '' || $stopcol eq '' || $idcol eq '') {
	die('Could not determine all the necessary columns');
}
else {
	print "\t=> Done, using: $chrcol, $startcol, $stopcol and $idcol\n";
}
##################################################
## SPECIFIC CODE FOR LIFTING THE PROBELOCATIONS ##
##################################################
my %newprobepos;
my $querypart;
if ($table eq 'probelocations') {
	$querypart = ', name';
	$stopcol = "$stopcol + 1";
	if ($opts{'p'} ) {
		print "Loading Provided new Probe Locations\n";
		my $string = $opts{'p'};
		my @pfiles = split(/@@/,$string);
		foreach(@pfiles) {
			open IN, $_;
			my $head = <IN>;
			chomp($head);
			my @cols = split(/;|,|\t/,$head);
			my $namecol;
			my $chrcol;
			my $poscol;
			$i = -1;
			foreach(@cols) {
				$i++;
				my $col = $_;
				if ($col =~ m/name/i) {
					$namecol = $i;
				}
				elsif ($col =~ m/chr/i) {
					$chrcol = $i;
				}
				elsif ($col =~ m/pos|mapinfo/i) {
					$poscol = $i;
				}
			}
			while (<IN>) {
				chomp($_);
				my @p = split(/;|,|\t/);
				if ($p[$chrcol] eq '0' || $p[$chrcol] eq '' || $p[$poscol] eq '0' || $p[$poscol] eq '' || $p[$chrcol] eq 'NULL' || $p[$poscol] eq 'NULL' ) {
					next;
				}
				$p[$chrcol] = $chromhash{$p[$chrcol]};
				$newprobepos{$p[$namecol]} = $p[$chrcol] . ";" .$p[$poscol];
			}
			close IN;
		}
		print "Stored ". keys(%newprobepos) . " probe positions\n";
	}
}

###########################
# DUMP DATA TO NEW TABLE  #
###########################
print "3/ Copy Data to new build database\n";
$dbh->do("TRUNCATE TABLE `$targetdb`.`$table`");
$query = "INSERT INTO `$targetdb`.`$table` SELECT * FROM `$sourcedb`.`$table` ";
$dbh->do($query);
print "\t=> Done\n";
##################
# LIFT THE DATA! #
##################
my $dumpfile = "Files/LiftOver_$table.region.chrIN";
my $chrdump = "Files/LiftOver_$table.region.chrIN";
my $chrdumpOK = "Files/LiftOver_$table.region.chrOK";
my $chrdumpBAD = "Files/LiftOver_$table.region.chrBAD";
my $load_all = "Files/LiftOver_$table.all.IN";


#my $query = "SELECT $chrcol, $startcol, $stopcol, $idcol, LiftedFrom $querypart FROM `$targetdb`.`$table` WHERE $chrcol = ? LIMIT 1000";
my $dumpquery = "SELECT CONCAT('chr',$chrcol), $startcol, $stopcol, $idcol FROM `$targetdb`.`$table`  WHERE $chrcol = ?";
my $fullQuery = "SELECT * FROM `$targetdb`.`$table` WHERE $chrcol = ?";

#$dbh->do("ALTER TABLE `$targetdb`.`$table` DISABLE KEYS");
$absth = $dbh->prepare($fullQuery);
$dumpsth = $dbh->prepare($dumpquery);
print "4/ Lifting the coordinate information\n";
my %faileditems;
our %masslift;
my $total;
my @keys;
open LOAD, ">$load_all" or die("Could not open loading table : $load_all\n");
my %p_x_c;
for ($i=1;$i<=25;$i++) {
	print "\nChr ".$chromhash{$i}." : ";
	# first perform mass lift for speed
	if (-e "$dumpfile") {
		unlink($dumpfile);
	}
    # fetch and dump to outfile
    open DUMP, ">$dumpfile" or die("Could not open dump file: $dumpfile\n");
   	my $max = $dumpsth->execute($i);
    if ($max > 100000) {
        $max = 100000;
    }
    my $rowcache;
    while (my $row = shift(@$rowcache) || shift( @{$rowcache = $dumpsth->fetchall_arrayref(undef,$max)|| []})) {
        print DUMP join("\t",@$row)."\n";
	}
    close DUMP;
	system("sed -e 's/chr23/chrX/g' -e 's/chr24/chrY/g' -e 's/chr25/chrM/g' '$chrdump' > '$chrdump.tmp.txt' && mv '$chrdump.tmp.txt'  '$chrdump'");

	my $rows = 0;
	$rows = $absth->execute($i);
	$total = $total + $rows;
   	system("./liftOver -minMatch=0.9 '$chrdump' '$chainfile' '$chrdumpOK' '$chrdumpBAD' > /dev/null 2>&1");
	%masslift = ();
	open IN, "$chrdumpOK";
	while (<IN>) {
		chomp($_);
		my @pieces = split(/\t/,$_);
		$pieces[0] =~ s/chr//;
		# key in masslift is identifier 'ID' from the 'idcol' in the table 
		$masslift{$pieces[3]} = $pieces[0]."\t".$pieces[1]."\t".$pieces[2];
	}
	close IN;
    print keys(%masslift) ." exacts lifts out of $rows items, updating tables & processing remaining items: ";
    
	# then fetch data row by row and update
	my $rowcache;
   	if ($rows < 10000) {
		$max = $rows;
   	}
   	else {
   	     $max = 10000;
   	}
   	my $item = 0;
   	my $perc = 10;
   	my $tresh = int(($rows / 100)*$perc);
    
	while (my $result = shift(@$rowcache) || shift( @{$rowcache = $absth->fetchall_arrayref({},$max) || []})) {
        if (scalar(@keys) == 0) {
            @keys = keys %$result;
            
        }
		$item++;
		if ($item > $tresh) {
			print " $perc%";
			$perc = $perc + 10;
			$tresh = int(($rows / 100) * $perc);
		}
	
		my $chr = $result->{$chrcol} ;#[0];
		my $chrtxt = $chromhash{$chr};
		my $start = $result->{$startcol} ; # [1];
		my $stop = $result->{$stopcol} ; # [2];
		my $size = $stop - $start + 1;
		my $id = $result->{$idcol} ; # [3];
		my $lifted = $result->{'LiftedFrom'} ; # [4];
		my $name = '';
        # escape values.
        foreach my $k (@keys) {
        		$result->{$k} =~ s/\t|\n/\|/g;
        }
			
		if ($table eq 'probelocations') {
			$name = $result->{'name'} ; # [5];
			if (exists($newprobepos{$name})) {
				my $val = $newprobepos{$name};
				my ($newchr,$newstart) = split(/;/,$val);
				if ($newchr =~ m/(X|Y|M|XY|MT)/i) {
					$newchr = $chromhash{$newchr};
				}
				if ($newchr == 0 || $newchr !~ m/\d+/ || $newstart == 0 || $newstart !~ m/\d+/) {
					open OUT, ">>/tmp/failedprobelift.txt";
					print OUT "probe $id ($name) => maps to chr$newchr : $newstart\n";
					close OUT;
					print "probe id : $id ($name) => maps to chr$newchr : $newstart\n";
				}
                			print LOAD "$id\t$newchr\t$newstart\t\n";
				#$dbh->do("UPDATE `$targetdb`.`$table` SET $chrcol = '$newchr', $startcol = '$newstart' , LiftedFrom ='' WHERE $idcol = '$id'");
				next;
			}
		}
		my ($status,$text,$newchrtxt,$newstart,$newstop) = &Lift($chrtxt,$start,$stop,$id);
		if ($status == 1) {
			# lift success
			# check size!
			my $newchr = $chromhash{$newchrtxt};
			my $newsize = $newstop - $newstart + 1;
			if ($newsize < $retain*$size) {
				$faileditems{$id} = "LiftOver Failed: Mapped region is smaller than minimal fraction ($retain) of original size.";  
			}
			elsif ($table eq 'probelocations') {
                $result->{$chrcol} = $newchr;
		        $result->{$startcol} = $newstart;
		        $result->{'LiftedFrom'} = $opts{'d'};
		        
		        my $line = join("\t",map { $result->{$_}} @keys);
		        print LOAD "$line\n";
			}
			elsif ($table eq 'aberration') {
        	    my $chipid;
		        if ($p_x_c{$result->{'idproj'}}){
		            $chipid = $p_x_c{$result->{'idproj'}};
		        } else {
				    $psth = $dbh->prepare("SELECT p.chiptypeid FROM `$sourcedb`.project p JOIN `$sourcedb`.aberration a ON a.idproj = p.id WHERE a.id = '$id'");
				    $psth->execute();
				    ($chipid) = $psth->fetchrow_array(); 
            	    $p_x_c{$result->{'idproj'}} = $chipid;
                }
			    $psth->finish();
			    # lift largestart & largestop
			    $psth = $dbh->prepare("SELECT position FROM `$targetdb`.probelocations WHERE chromosome = '$newchr' AND chiptype = '$chipid' AND position < '$newstart' ORDER BY position DESC LIMIT 1");
			    $psth->execute();
			    my ($largestart) = $psth->fetchrow_array();
			    $psth->finish();
			    if ($largestart eq '') {
				    $largestart = '0';
			    }
			    $psth = $dbh->prepare("SELECT position FROM `$targetdb`.probelocations WHERE chromosome = '$newchr' AND chiptype = '$chipid' AND position > '$newstop' ORDER BY position ASC LIMIT 1");
			    $psth->execute();
			    my ($largestop) = $psth->fetchrow_array();
			    $psth->finish();
			    if ($largestop eq '') {
				    $largestop = 'ter';
			    }
 			    # update the values
		        $result->{$chrcol} = $newchr;
            	$result->{$startcol} = $newstart;
		        $result->{$stopcol} = $newstop;
		        $result->{'largestart'} = $largestart;
		        $result->{'largestop'} = $largestop;
		        $result->{'LiftedFrom'} = $opts{'d'};
		        my $line = join("\t",map { $result->{$_}} @keys);
		        print LOAD "$line\n";
            	
			}
			else {
                # update the values
		        $result->{$chrcol} = $newchr;
		        $result->{$startcol} = $newstart;
		        $result->{$stopcol} = $newstop;
				$result->{'LiftedFrom'} = $opts{'d'};
		        my $line = join("\t",map { $result->{$_}} @keys);
		        print LOAD "$line\n";
			}
		}
		else {
			$faileditems{$id} = $text;
		}
		
   	}
	print " Done.";
}
# close table
close LOAD;
# load to db.
$dbh->do("TRUNCATE TABLE `$targetdb`.`$table`");
my $query = "LOAD DATA LOCAL INFILE '$load_all' INTO TABLE `$targetdb`.`$table` (`".join("`,`",@keys)."`)";
$dbh->do($query);
my $sr = ($total - keys(%faileditems)) / $total;
$sr = sprintf("%.2f", ($sr*100));
print "\n\t=> Finished with a successrate of $sr%\n";
print "\t=> Storing " . keys(%faileditems) . " failed items for curation\n";
############################
# PROCESS THE FAILED ITEMS #
############################
my %owners = ();
my %skip = ();
my $noproject = 0;
my $skipped = 0;
if ($table =~ m/aberration|parents/) {
	if ($table =~ m/cus_/) {
		$cus="cus_";
	}
	else {
		$cus = '';
	}
	# first load hash of project ownerships
	# we will send curation jobs to project owners
	my $psth = $dbh->prepare("SELECT id, naam, collection, userID FROM $cus"."project");
	$psth->execute();
	while (my @row = $psth->fetchrow_array()) {
		$owners{$row[0]} = $row[3];
		# SKIP the hapmap control projects.  It is better to update the datafiles and re-analyze them from scratch !
		if ($row[1] =~ m/HapMap/i && $row[2] eq 'Controls') {
			$skip{$row[0]} = 1;
		}
	} 
}

if ($table =~ m/aberration/) {
	my $subquery = "";
	if ($table =~ m/cus_/) {
		$cus="cus_";
		$subquery = "SELECT cn, start, stop, chr, largestart, largestop, sample, idproj, inheritance, class, confidence, platform, chiptype, nrprobes, LiftedFrom FROM `$sourcedb`.`cus_aberration` a WHERE a.id = ?";
		my $substh = $dbh->prepare($subquery);
 		for my $key (keys(%faileditems)) {
			$substh->execute($key);
			my @row = $substh->fetchrow_array();
			my $class = $row[9];
			my $sid = $row[6];
			my $pid = $row[7];
			my $chr = $row[3];
			my $start = $row[1];
			my $stop = $row[2];
			my $cn = $row[0];
			my $inh = $row[8];
			my $conf = $row[10];
			my $largestart = $row[4];
			my $largestop = $row[5];
			my $platform = $row[11];
			my $chiptype = $row[12];
			my $nrprobes = $row[13];
			my $LiftedFrom = $row[14];
			if (!exists($owners{$pid})) {
				# there are some orphan CNVs in the database that don't belong to any project. skip them.
				$noproject++;
				next;
			}
			my $owner = $owners{$pid};
			if ($skip{$pid} == 1) {
				# this is a hapmap control CNV, skipping
				$skipped++;
				next;
			}
			my $arguments = $faileditems{$key}; 
			$dbh->do("INSERT INTO `$targetdb`.`cus_deletedcnvs` (id, cn, start, stop, chr, sample, idproj, inheritance, class,largestart, largestop, confidence, platform, chiptype, nrprobes, LiftedFrom, FinalBuild) VALUES ('$key','$cn','$start','$stop','$chr','$sid','$pid','$inh','$class','$largestart','$largestop','$conf','$platform','$chiptype','$nrprobes','$LiftedFrom','".$opts{'d'}."')");
			$logentry = "Deleted";
			$dbh->do("INSERT INTO `$targetdb`.`cus_log` (sid, pid, aid, uid, entry,arguments) VALUES ('$sid', '$pid', '$key','$uid', '$logentry', '$arguments')");
			#$dbh->do("DELETE FROM `$targetdb`.`cus_aberration` WHERE id = '$key'");
			$dbh->do("INSERT INTO `GenomicBuilds`.`FailedLifts` (frombuild, tobuild, tablename, itemID, uid, Reason) VALUES ('".$opts{'d'}."','".$opts{'D'}."','$table','$key','$owner','$arguments')");
		}
	}
    elsif ($table =~ m/_LOH/ || $table =~ m/_mosaic/) {
		$subquery = "SELECT a.idproj FROM `$sourcedb`.`$table` a WHERE a.id = ? ";
		# move to deleted table and insert log item !
		my $substh = $dbh->prepare($subquery);
		for my $key (keys(%faileditems)) {
			$substh->execute($key);
			my @row = $substh->fetchrow_array();
			my $pid = $row[0];
			if (!exists($owners{$pid})) {
				# there are some orphan CNVs in the database that don't belong to any project. skip them.
				$noproject++;
				next;
			}
			my $owner = $owners{$pid};
			if ($skip{$pid} == 1) {
				# this is a hapmap control CNV, skipping
				$skipped++;
				next;
			}

			my $arguments = $faileditems{$key};
            $dbh->do("INSERT INTO `GenomicBuilds`.`FailedLifts` (frombuild, tobuild, tablename, itemID, uid, Reason) VALUES ('".$opts{'d'}."','".$opts{'D'}."','$table','$key','$owner','$arguments')");
			$logentry = "Deleted";
			#$dbh->do("INSERT INTO `$targetdb`.`log` (sid, pid, aid, uid, entry,arguments) VALUES ('$sid', '$pid', '$key','$uid', '$logentry', '$arguments')");
			#$dbh->do("DELETE FROM `$targetdb`.`aberration` WHERE id = '$key'");
			#$dbh->do("INSERT INTO `GenomicBuilds`.`FailedLifts` (frombuild, tobuild, tablename, itemID, uid, Reason) VALUES ('".$opts{'d'}."','".$opts{'D'}."','$table','$key','$owner','$arguments')");
		}

	}
    
	else {
		$subquery = "SELECT a.sample, a.idproj, a.class, a.chr, a.start, a.stop, a.cn, a.inheritance, a.seenby, a.LiftedFrom FROM `$sourcedb`.`aberration` a WHERE a.id = ? ";
		# move to deleted table and insert log item !
		my $substh = $dbh->prepare($subquery);
		for my $key (keys(%faileditems)) {
			$substh->execute($key);
			my @row = $substh->fetchrow_array();
			my $class = $row[2];
			my $sid = $row[0];
			my $pid = $row[1];
			my $chr = $row[3];
			my $start = $row[4];
			my $stop = $row[5];
			my $cn = $row[6];
			my $inh = $row[7];
			my $sb = $row[8];
			my $LiftedFrom = $row[9];
			if (!exists($owners{$pid})) {
				# there are some orphan CNVs in the database that don't belong to any project. skip them.
				$noproject++;
				next;
			}
			my $owner = $owners{$pid};
			if ($skip{$pid} == 1) {
				# this is a hapmap control CNV, skipping
				$skipped++;
				next;
			}

			my $arguments = $faileditems{$key};
			$dbh->do("INSERT INTO `$targetdb`.`deletedcnvs` (id, cn, start, stop, chr, sample, idproj, inheritance, class, seenby, LiftedFrom, FinalBuild) VALUES ('$key','$cn','$start','$stop','$chr','$sid','$pid',0,'$class','$sb','$LiftedFrom','".$opts{'d'}."')");
			$logentry = "Deleted";
			$dbh->do("INSERT INTO `$targetdb`.`log` (sid, pid, aid, uid, entry,arguments) VALUES ('$sid', '$pid', '$key','$uid', '$logentry', '$arguments')");
			#$dbh->do("DELETE FROM `$targetdb`.`aberration` WHERE id = '$key'");
			$dbh->do("INSERT INTO `GenomicBuilds`.`FailedLifts` (frombuild, tobuild, tablename, itemID, uid, Reason) VALUES ('".$opts{'d'}."','".$opts{'D'}."','$table','$key','$owner','$arguments')");
		}
	}
}
else {
	# delete from target table, store in failedLift table for manual curation
	for my $key (keys(%faileditems)) {
		#$dbh->do("DELETE FROM `$targetdb`.`$table` WHERE id = '$key'");
		my $arguments = $faileditems{$key};
		# owner if table is parents related: parents_upd, parents_regions    
		my $owner = $uid;
		if ($table eq 'parents_regions') {
			my $ssth = $dbh->prepare("SELECT sid FROM `$sourcedb`.`$table` WHERE id = '$key'");
			$ssth->execute();
			my @r = $ssth->fetchrow_array();
			$ssth->finish();
			my $sid = $r[0];
			$ssth = $dbh->prepare("SELECT father, father_project, mother, mother_project FROM `$sourcedb`.`parents_relations` WHERE father = '$sid' OR mother = '$sid'");
			$ssth->execute();
			my @r = $ssth->fetchrow_array();
			$ssth->finish();
			my $pid = 0;
			if ($r[0] == $sid) {
				$pid = $r[1];
			}
			elsif ($r[2] == $sid) {
				$pid = $r[3];
			}
			if ($pid != 0 && exists($owners{$pid})) {
				$owner = $owners{$pid};
			}
		}
		elsif ($table eq 'parents_upd') {
			my $ssth = $dbh->prepare("SELECT spid FROM `$sourcedb`.`$table` WHERE id = '$key'");
			$ssth->execute();
			my @r = $ssth->fetchrow_array();
			$ssth->finish();
			if ($r[0] != 0 && exists($owners{$r[0]})) {
				$owner = $owners{$r[0]};
			}
		}
        elsif ($table eq 'Filters_x_Regions') {
            my $ssth = $dbh->prepare("SELECT f.uid FROM `$sourcedb`.Filters f JOIN `$sourcedb`.Filters_x_Regions fr ON f.fid = fr.fid WHERE fr.rid = '$key'");
			$ssth->execute();
			my @r = $ssth->fetchrow_array();
			$ssth->finish();
			if ($r[0] != 0 && exists($owners{$r[0]})) {
				$owner = $owners{$r[0]};
			}
        }
        elsif ($table eq 'Classifier_x_Regions') {
            my $ssth = $dbh->prepare("SELECT c.uid FROM `$sourcedb`.Classifier c JOIN `$sourcedb`.Classifier_x_Regions cr ON c.id = cr.cid WHERE cr.id = '$key'");
			$ssth->execute();
			my @r = $ssth->fetchrow_array();
			$ssth->finish();
			if ($r[0] != 0 && exists($owners{$r[0]})) {
				$owner = $owners{$r[0]};
			}
        }
		$dbh->do("INSERT INTO `GenomicBuilds`.`FailedLifts` (frombuild, tobuild, tablename, itemID, uid, Reason) VALUES ('".$opts{'d'}."','".$opts{'D'}."','$table','$key','$owner','$arguments')");
	}
}
if ($skipped > 0) {
	my $left = keys(%faileditems) - $skipped;
	print "\t => Skipped $skipped Regions from HapMap Control Samples, $left items to do manually. \n";
}
print "\n\n => DONE!\n";
if ($table eq 'probelocations') {
	system("echo 0 > /$sitedir/status/LiftOver.status");
}



###############
# SUBROUTINES #
###############

sub Lift {
	# returns array of STATUS(0=failed,1=OK),REASON(if failed) or Chr,Start,Stop(if OK)
    #print("key ins masslift: ".keys(%masslift)."\n");
   	my($chr, $start, $stop,$id) = @_;
	#print "  Chr$chr:$start-$stop ($id) : ";
    
	if ($masslift{$id}) {
		my @parts = split(/\t/,$masslift{$id});
		#print "Masslift : Success.\n";
		return (1,'ok',$parts[0],$parts[1],$parts[2]);
	}
	else {
        #return (0,$reason,'','','');
		my $infile = "Files/LiftOver_$table.$id.region.in";
		my $outOK = "Files/LiftOver_$table.$id.region.outOK";
		my $outBAD = "Files/LiftOver_$table.$id.region.outBAD";
		my $outMerged = "Files/LiftOver_$table.$id.region.outMerged";
		open OUT, ">$infile";
		print OUT "chr$chr\t$start\t$stop\t$id";
		close OUT;
		system("./liftOver -minMatch=0.9 -multiple '$infile' '$chainfile' '$outOK' '$outBAD' > /dev/null 2>&1");
		if (-z $outOK) {
			# lift failed
			my $reason = `head -n 1 $outBAD`;
			chomp($reason);
			$reason =~ s/#//;
			$reason = "LiftOver Failed: $reason";
			#print "chr$chr:$start-$stop : $reason\n";
			#print "Failure 1.\n";
            system("rm -f Files/LiftOver_$table.$id.region*") == 0 or die("Could not clean up liftover tmp files for id $id\n");
			return (0,$reason,'','','');
		}
		else {
			# lift ok
			system("./liftOverMerge -mergeGap=$Gap '$outOK' '$outMerged' && mv '$outMerged' '$outOK' > /dev/null 2>&1");
			open IN, "$outOK";
			my @lines = <IN>;
			if (scalar(@lines) > 1) {
				# region is split => Failed
				#print "Failure allowing splits\n";
                system("rm -f Files/LiftOver_$table.$id.region*") == 0 or die("Could not clean up liftover tmp files for id $id\n");
				return(0,'LiftOver Failed: Region is split in new build','','','');
			}
			else {
				my $line = $lines[0];
				chomp($line);
				my @parts = split(/\t/,$line);
				$parts[0] =~ s/chr//;
				#print "Success using split lift\n";
                system("rm -f Files/LiftOver_$table.$id.region*") == 0 or die("Could not clean up liftover tmp files for id $id\n");
				return(1,'ok',$parts[0],$parts[1],$parts[2]);
			}		
		}
	}	
}









