#!/usr/bin/perl

use Sys::CPU;
use threads;
use Thread::Queue;
use threads::shared;
use Sys::CpuLoad;
use Number::Format;
use Getopt::Std;
use DBI;

# read credentials
use Cwd 'abs_path';
$credpath = abs_path("../LoadCredentials.pl");
require($credpath);

$| = 1;

# DEFINE VARIABLES
my %chromhash = ();
my @chroms = (1..22);
foreach (@chroms)  {
	$chromhash{ $_ } = $_;
}
$chromhash{ "23" } = "X";
$chromhash{ "24" } = "Y"; 
$chromhash{ "X" } = "23";
$chromhash{ "Y" } = "24";
$chromhash{ "M" } = "25";
$chromhash{ "25" } = "M";

##########################
# COMMAND LINE ARGUMENTS #
##########################
# L = Looking at a new database, LiftOver old values if needed
# D = Use specific database (mandatory if L is specified), otherwise use default.
# d = Database to use as source (mandatory if L is specified.
# u = User ID, mandatory


getopts('LD:d:u:', \%opts);  # option are in %opts

print "1/ Connecting to the database\n";
my $db = "";

if ($opts{'D'}){
	$db = "CNVanalysis".$opts{'D'};
	$targetdb = $opts{'D'};
	print "\t=> Using user specified database $db\n";
}
else {
	# connect to GenomicBuilds DB to get the current Genomic Build Database. 
	my $dbhgb = DBI->connect("dbi:mysql:GenomicBuilds:$host",$userid,$userpass);
	my $gbsth =$dbhgb->prepare("SELECT name, StringName FROM CurrentBuild");
	$gbsth->execute();
	my @row = $gbsth->fetchrow_array();
	$gbsth->finish();
	$db = "CNVanalysis".$row[0];
	$newbuild = $row[1];
	print "\t=> Using current build database (build ".$row[1].")\n";	
}
$connectionInfocnv="dbi:mysql:$db:$host"; 
$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) ;
if ($opts{'d'}) {
	$sourcedb = $opts{'d'};
}
if ($opts{'u'}) {
	$uid = $opts{'u'};
}
else {
	$uid = 1;
}

# GET THE SCRIPTS TO RUN
my $query = "SELECT DISTINCT(UpdateScript) FROM `$db`.`Administration`";
my $sth = $dbh->prepare($query);
my $rows = $sth->execute();
$i = 0;
while (my @row = $sth->fetchrow_array()) {
	$i++;
	$script = $row['0'];
	$outname = $script;
	$outname =~ s/\.(pl|py)$//;
	print "Starting $script\n";
    print "(cd $maintenancedir/Annotation_scripts && ./$script -d '$sourcedb' - D '$targetdb' -L -u '$uid') > $maintenancedir/LiftOver/Files/$outname.out 2>&1\n";
	${thr."$i"} = threads->create(sub{system("(cd $maintenancedir/Annotation_scripts && ./$script -d '$sourcedb' -D '$targetdb' -L -u '$uid') > $maintenancedir/LiftOver/Files/$outname.out 2>&1")} );
	#unlink("Files/$outname.out");
	#print "$script Finished!\n";
}
for ($j = 1; $j<=$i; $j++) {
	${thr."$j"}->join();	
} 

# finished 

system("echo 0 > /$sitedir/status/LiftOver.status");
